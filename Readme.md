# Poop

> An unfinished script interpreter written in PHP

## Disclaimer

This is testing code that is not fully functional. It doesn't implement everything it promises and should generally not be used by anyone.

## Overview

It was a rainy day in London in November 2011 when I had the inclination to try and write an interpreter for a simple scripting language. As everyone knows, the best language to make this interpreter in is PHP. So that's what I started doing. When i sobered up, I realised what a stupid idea it was and abandoned my efforts. That's this repository. Goodbye!

## Install / Clone

```bash
git clone https://gitlab.com/g4z/poop.git
cd poop # Change into the poop directory
```

## Using it

### Run interactive shell

```bash
php src/poop.php
```

### Run a script

```bash
php src/poop.php -f examples/example1.poop
```

### Use it as a library

```php
require 'poop.php';
$poop = new Poop\Run;
$poop->setCode('x = 23; x = x+2; echo "x=", x;');
$result = $poop->run();
$result->dump();
```

## Example interactive use

*Note: Most of the command line options/flags are not implemented*

```bash
[user@pc ~/poop]$ php src/poop.php -h

Usage: poop.php [option1[=arg1]] [option2[=arg2]] ...

Options:

 -c --code=CODE  Source code string
 -d --debug      Enable debugging
 -f --file=FILE  Path to code file
 -h --help       Display program help
 -g --gui        Display the web GUI
 -m --minimise   Output minimised code
 -s --syntax     Perform a syntax check
 -t --tokenise   Output tokenised code

[user@pc ~/poop]$ php src/poop.php -c 'a=23; if(a>22) { echo "Yes it is"; }'
Yes it is

[user@pc ~/poop]$ php src/poop.php
poop> help

 Commands:   dump, exit, help
 Keywords:   break, echo, if, quit, while
 Functions:  file.read, file.write, file.append, file.prepend, file.touch, file.remove, http.get, math.ceil, math.floor, str.repeat, str.replace, str.trim

poop> myfile = "/tmp/this-is-my-file.tmp";
poop> file.touch(myfile);
poop> file.append(myfile, "Hello World");
poop> echo file.read(myfile);
Hello World
poop> myIpAddr = http.get("http://ip.appspot.com/");
poop> echo myIpAddr;
79.52.11.178
poop> exit
```
