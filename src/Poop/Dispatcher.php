<?php
namespace Poop;
/**
* Implementation of Dispatcher_API
* @package Poop
*/
class Dispatcher
extends Interpreter
implements Dispatcher_API
{
    private $runnable = FALSE;

    /**
    * Construct an instance
    * @param array $options Optional array of name/value options
    * @see Options
    */
    public function __construct(array $options = NULL) {
        try {
            parent::__construct($options);
            $this->runnable = TRUE;
        } catch(OptionException $e) {
            $result = new RunResult();
            $result->setStatus(Result::FAIL);
            $result->addError($e);
            $this->doOutput($result);
        }
    }

    /**
    * Dispatch the correct action based on Environment and Options
    * @return Result
    */
    public function run()
    {
        if (!$this->runnable) {
            return 1;
        }

        $this->debug(__METHOD__, __LINE__);

        if ($this->getOption(array('help', 'h'))) {
            $this->doHelp();
            return;
        } elseif ($this->getOption(array('tokenise', 't'))) {
            $result = $this->doTokenise();
        } elseif ($this->getOption(array('syntax', 's'))) {
            $result = $this->doSyntaxCheck();
        } elseif ($this->getOption(array('minimise', 'm'))) {
            $result = $this->doMinimise();
        } else {
            if ($this->getCode()) {
                $this->debug(__METHOD__, __LINE__, 'Entering scripted mode');
                $result = $this->doRun();
            } else {
                if ($this->environment->isCli()) {
                    $this->debug(__METHOD__, __LINE__, 'Entering interactive mode');
                    $result = $this->doShell();
                } else {
                    $this->doHelp();
                    return;
                }
            }
        }

        if ($this->environment->isHosted()) {
            $this->debug(__METHOD__, __LINE__, 'Environment is PHP included');
            return $result;
        }

        $this->doOutput($result);

        // $this->debug(__METHOD__, __LINE__, 'Memory', $this->memory->getData());

        // TODO: retval from here
        return 0;

    }
}