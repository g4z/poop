<?php
namespace Poop;
/**
 * General syntax error exception
 * @package Poop
 */
class SyntaxException 
extends Exception
implements SyntaxException_API
{
    /**
     * @var int Source code line number of the error
     */
    protected $line;

    /**
     * @var int Source code column number of the error
     */
    protected $column;

    /**
     * @var int Internal source code filename of the error
     */
    protected $internal_filename;

    /**
     * @var int Internal source code line number of the error
     */
    protected $internal_line;


    public function __toString() {
        return 'SyntaxException [' . $this->getLineNumber() . '.' . 
            $this->getColumnNumber() . '] ' . $this->getMessage() . 
            ' (@' . basename($this->getInternalFilename()). ':' . 
            $this->getInternalLineNumber(). ')';
    }
    /**
     * Construct this syntax exception
     * @param int $line The line number of the syntax error
     * @param int $column The column number of the syntax error
     * @param string $message The error message string
     */
    public function __construct($line, $column, $message, 
                            $internal_filename, $internal_line) {
        parent::__construct($message);
        $this->line = $line;
        $this->column = $column;
        $this->internal_line = $internal_line;
        $this->internal_filename = $internal_filename;
    }

    /**
     * Return the line number of the error
     */
    public function getLineNumber() { return $this->line; }
    
    /**
     * Return the column number of the error
     */
    public function getColumnNumber() { return $this->column; }

    /**
    * Return the line number from the php code (eg. __LINE__)
    * @return mixed
    */
    function getInternalFilename() { return $this->internal_filename; }

    /**
    * Return the line number from the php code (eg. __LINE__)
    * @return mixed
    */
    function getInternalLineNumber() { return $this->internal_line; }

}