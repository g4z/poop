<?php
namespace Poop;
/**
* Interface definition for a Type object
* @package Poop
*/
interface TokenType_API extends Type_API {

    /**
    * Represents the SYMBOL type
    */
    const SYMBOL = 1024;

    /**
    * Represents the IDENTIFIER type
    */
    const IDENTIFIER = 2048;

    /**
    * Represents the KEYWORD type
    */
    const KEYWORD = 4096;

    /**
    * Represents the MINVOKE (method invoke) type
    */
    const MINVOKE = 8192;

    /**
    * Represents the COMMENT type
    */
    const COMMENT = 16384;

}
