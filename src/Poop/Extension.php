<?php
namespace Poop;

use Poop\ArgumentException;
use Poop\Type;

/**
* Implement the Extension class
* @package Poop
*/
abstract class Extension
extends Object
// implements Extension_API
{
    /**
     * Stores a reference to the "parent" application
     */
    private $parent;

    /**
     * Construct the extension object
     * @param Interpreter $parent Reference to the parent application
     */
    public function __construct(Interpreter $parent) {
        // $this->debug(__METHOD__, __LINE__, 'Initialising extension: ' . get_class($this));
        $this->parent = $parent;
    }

    /**
     * Return a list of available methods for the scripting lang
     */
    public function exports() {
        $exports = array();
        foreach (get_class_methods($this) as $fn) {
            if (mb_substr($fn, 0, 3) == 'FN_') {
                $name = substr($fn, 3);
                $exports[] = $name;
            }
        }
        // $this->debug(__METHOD__, __LINE__, $exports);
        return $exports;
    }

    /**
     * Generic argument parser based on rules
     * @param array $arguments The arguments to parse
     * @param integer $total Expected number of arguments
     * @param array $rules Array of rules (eg. array(Type::INTEGER, Type::STRING))
     */
    protected function parseArguments(array $arguments, $total, array $rules) {

        // Get the script lang name of the calling class and method
        list(, $x) = debug_backtrace(NULL, 2);
        $method = strtolower(substr($x['class'], strrpos($x['class'], '\\') + 1))
            . '.' . strtolower(str_replace('FN_', '', $x['function']));
        unset($x);

        // Check we have the correct number of arguments
        if (count($arguments) !== $total) {
            throw new ArgumentException("$method: requires $total parameter(s)");
        }

        for ($i = 0; $i < $total; $i++) {
            $expected = $rules[$i];
            $actual = Type::convert($arguments[$i]);
            // if (($actual & $expected) === $actual) {
            if ($actual !== $expected) {
                throw new ArgumentException(
                    $method . ': paramater #' . ($i + 1) . 
                        ' must be type ' . Type::map($expected) . 
                        '; not type ' . Type::map($actual)
                );
            }
            $retval[] = $arguments[$i];
        }

        return $retval;
    }

    /**
     * Return a reference to the parent application
     * @return Interpreter
     */
    protected function getParent() {
        return $this->parent;
    }

}
