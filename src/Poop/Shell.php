<?php
namespace Poop;
/**
* Implementation of a CLI shell
* @package Poop
*/
class Shell
extends RunResult
implements Shell_API
{
    /**
    * @var bool $eof Set to true to exit the shell
    */
    protected $eof;
    /**
    * @var string $prompt Defines the shell prompt string
    * @see Shell::PROMPT The default prompt string
    */
    protected $prompt;

    /**
    * Construct the Shell object
    */
    public function __construct() {
        $this->eof = FALSE;
        $this->prompt = self::PROMPT;
    }

    /**
    * Return true if we should exit the shell
    * @return bool
    */
    public function isEOF() {
        return ($this->eof == TRUE);
    }

    /**
    * Read a line from the command prompt
    * @return string
    */
    public function readInput() {
        $line = readline($this->prompt);
        if (!empty($line)) {
            readline_add_history($line);
        }
        return $line;
    }

    /**
    * Output a string
    * TODO: error checking for file operation
    * @param string $buffer The string to output
    * @return Shell
    */
    public function writeOutput($buffer) {
        fputs(self::STDOUT, $buffer);
        return $this;
    }
}