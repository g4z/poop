<?php
namespace Poop;
/**
* Interface definition for the base object
* @package Poop
*/
interface Object_API {
    
    /**
    * Return a string representation of something and exit (debug)
    * @param mixed $data Data to dump
    * @return string A string representation of this class
    */
    function repr($data = NULL);

    /**
    * Print this object's string representation and exit (debug)
    * @param mixed $data The data to dump
    */
    function dump($data = NULL);

    /**
    * List methods available in object (debug)
    */
    function help();

    /**
    * Global debugging output method (debug)
    * @param  $method The value of the calling __METHOD__
    * @param  $line The value of the calling __LINE__
    * @param  $args The data to debug
    */
    function debug($method, $line, $args);

    /**
    * Global info logging
    * @param string $message The message to output to the logfile
    */
    function info($message);

    /**
    * Global warning logging
    * @param string $message The message to output to the logfile
    */
    function warning($message);

    /**
    * Global error logging
    * @param string $message The message to output to the logfile
    */
    function error($message);
}
