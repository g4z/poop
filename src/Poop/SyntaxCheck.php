<?php
namespace Poop;
/**
* Implementation of SyntaxCheck
* @package Poop
*/
class SyntaxCheck
extends Interpreter
implements SyntaxCheck_API
{
    /**
    * Construct an instance
    * @param array $options Optional array of name/value options
    * @see Options
    */
    public function __construct(array $options = NULL) {
        parent::__construct($options);
    }

    /**
    * Run this action and return a result object
    * @return Result
    */
    public function run() {
        return $this->doAction('SyntaxCheck');
    }
}