<?php
namespace Poop;
/**
* Implementation of Engine class
* @package Poop
*/
class Engine
extends Parser
implements Engine_API
{
    /**
    * Options for Engine
    * @var array Array of name/value options
    * @see Options
    */
    protected $options;

    /**
    * Construct the Engine object
    * @var array Array of name/value options
    */
    public function __construct(array $options = NULL) {
        parent::__construct();
        $this->options = new Options($options);
        $this->startup();
    }

    /**
    * Initialise the Engine object from user input
    * @throw Exception
    * @private
    */
    private function startup() {
        
        // =================== CONFIGURE LOGFILE ====================
        
        if ($this->environment->isCli()) {
            // TODO: Pass in debuglog filepath here
            $logfile = './poop.log';
        } else {
            $logfile = sys_get_temp_dir() . '/poop.log';
        }
        Log::instance()->setFilepath($logfile);

        // ================ PROCESS STARTUP OPTIONS =================
        
        /*  $opt = [0] => file [1] => f 
        [2] => 1 [3] => Path to code file */
        foreach (OptionMap::getInstance() as $opt) {
            switch ($opt[0])
            {
                case 'code':
                {
                    // Code was passed as --code argument
                    if ($option = $this->getOption(array($opt[0], $opt[1]))) {
                        if ($option === true) { // empty --code arg passed
                            $this->setBuffer('');
                        } else {
                            $this->setBuffer($option);
                        }
                    }
                }
                break;
                case 'file':
                {
                    if ($option = $this->getOption(array($opt[0], $opt[1]))) {
                        if (!is_readable($option)) {
                            throw new OptionException('--file not readable: ' . $option);
                        }
                        $this->setBuffer(file_get_contents($option));
                    }
                }
                break;
                case 'debug':
                {
                    if ($option = $this->getOption(array($opt[0], $opt[1]))) {
                        Log::instance()->enableDebug();
                    }
                }
                break;
                default:
                break;
            }
        }

        // $this->dump();

        // ================== INITIAL LOGFILE OUTPUT ======================

        $this->debug(__METHOD__, __LINE__);
        $this->debug(__METHOD__, __LINE__, '======[       O_O     ]======');
        $this->debug(__METHOD__, __LINE__);
        $this->debug(__METHOD__, __LINE__, "Using logfile: $logfile");
        if (Log::instance()->debugEnabled()) {
            $this->debug(__METHOD__, __LINE__, "Debugging is ENABLED");
        }

        // ================== LOAD EXTENSIONS ======================
        $total = $this->getExtensionManager()->load();
        $this->debug(__METHOD__, __LINE__, sprintf(
            'Loaded %u extensions',
            $total
        ));

    }

    /**
    * Set the source code string buffer
    * @param string $code The source code string
    * @return Engine
    */
    public function setCode($code) {
        $this->setBuffer($code);
        $this->setOption('code', $code);
    }

    /**
    * Get the source code string buffer
    * @return string
    */
    public function getCode() {
        return $this->getBuffer();
    }

    /**
    * Wrapper for reading STDIN
    * @param string $prompt Optional prefixed "command prompt" string
    * @return string
    */
    protected function stdin($prompt = '') {
        $line = readline($prompt);
        if (!empty($line)) {
            readline_add_history($line);
        }
        return $line;
    }

    /**
    * Wrapper for writing to STDOUT
    * @param string $buffer
    */
    protected function stdout($buffer) {
        fwrite(STDOUT, $buffer);
    }

    /**
    * Wrapper for writing to STDERR
    * @param string $buffer
    */
    protected function stderr($buffer) {
        fwrite(STDERR, $buffer);
    }

    /**
    * Handle the execute action
    * @return RunResult
    */
    public function doRun() {
        $this->debug(__METHOD__, __LINE__);
        $result = new RunResult();
        try {
            //ob_start();
            if (mb_strlen($this->getCode())) {
                $this->parse();
            }
        } catch(EofException $e) {
        } catch(Exception $e) {
            $this->error($e);
            $result->setStatus(Result::FAIL);
            $result->addError($e);
        }
        // $result->setOutput(ob_get_contents());
        // ob_end_clean();
        $result->setData($this->getMemory()->getData());
        return $result;
    }

    /**
    * Run the syntax check action
    * @return SyntaxCheckResult
    */
    public function doSyntaxCheck() {

        $this->debug(__METHOD__, __LINE__);
        $result = new SyntaxCheckResult();
        
        // TODO: Fix me
        $result->addError(new Exception("SyntaxCheck is not working!"));
        return $result;

        try {
            while ($token = $this->getToken()) {}
        } catch(EofException $e) {
        } catch(Exception $e) {
            $result->clearData();
            $result->setStatus(Result::FAIL);
            $result->addError($e);
        }
        return $result;
    }

    /**
    * Run the tokeniser action
    * @return TokeniseResult
    */
    public function doTokenise() {
        $this->debug(__METHOD__, __LINE__);
        $result = new TokeniseResult();
        try {
            while ($token = $this->getToken()) {
                $result->addData($token->toArray());
            }
        } catch(EofException $e) {
        } catch(Exception $e) {
            $result->clearData();
            $result->setStatus(Result::FAIL);
            $result->addError($e);
        }
        return $result;
    }

    /**
    * Run the minimiser action
    * @return MinimiseResult
    */
    public function doMinimise() {

        $this->debug(__METHOD__, __LINE__);
        $result = new MinimiseResult();

        // TODO: Fix me
        $result->addError(new Exception("Minimise is not working!"));
        return $result;

        try {
            while ($token = $this->getToken()) {
                $result->addData($token->toArray());
            }
        } catch(EofException $e) {
        } catch(Exception $e) {
            $result->clearData();
            $result->setStatus(Result::FAIL);
            $result->addError($e);
        }
        return $result;
    }

    /**
    * Return the engine Memory object
    * @return Memory
    */
    // public function getMemory() { return $this->memory; }

    /**
    * Return the engine Parser object
    * @return Parser
    */
    // public function getParser() { return $this->parser; }
    
    /**
    * Return the engine options
    * @return Options
    */
    public function getOptions() { return $this->options; }

    /**
    * Return an engine option
    * @param array $names Array of option names
    * @return mixed
    */
    public function getOption(array $names) {
        foreach ($names as $name) {
            if ($this->options->hasOption($name)) {
                $option = $this->options->getOption($name);
                return $option;
            }
        }   
    }

    /**
    * Return the engine options
    * @param string $name Name of the option
    * @param mixed $value Value for the option
    * @return Options
    */
    public function setOption($name, $value) {
        $this->options->setOption($name, $value);
        return $this;
    }

}
