<?php
namespace Poop;
/**
* Implementation of Minimise
* @package Poop
*/
class Minimise
extends Interpreter
implements Minimise_API
{
    /**
    * Construct an instance
    * @param array $options Optional array of name/value options
    * @see Options
    */
    public function __construct(array $options = NULL) {
        parent::__construct($options);
    }

    /**
    * Run this action and return a result object
    * @return MinimiseResult
    */
    public function run() {
        return $this->doAction('Minimise');
    }
}
