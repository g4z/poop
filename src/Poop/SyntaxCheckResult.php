<?php
namespace Poop;
/**
* Syntax check result class
* @package Poop
*/
class SyntaxCheckResult
extends Result 
implements SyntaxCheckResult_API {
    /**
    * Override the default addData to generate string output
    * @param mixed $data
    * @return Result
    */
    public function addData($data)
    {
        $this->addOutput(sprintf(
            "LINE:%06u COL:%06u TYPE:%-12s %s\n",
            $data['line'],
            $data['column'],
            $data['type'],
            $data['value']
        ));
        return parent::addData($this);
    }
}