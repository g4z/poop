<?php
namespace Poop;
/**
* Log logger/debug handler object
* @package Poop
*/
class Log
implements Log_API
{
    /**
     * Debug flag (default: disabled)
     * @see self::enableDebug()
     * @see self::disableDebug()
     */
    private $debugEnabled = FALSE;

    /**
     * Path to the logfile
     */
    private $filepath = 'poop.log';

    /**
    * Prevert external instantiation
    */
    protected function __construct() {}

    /**
    * Return a singleton instance
    * @return Log
    */
    public static function instance() {
        static $instance = NULL;
        if (is_null($instance)) {
            $instance = new self;
        }
        return $instance;
    }

    /**
    * Write a string message to the logfile
    * @param  $buffer Reference to the buffer to log
    * @return bool
    */
    private static function doWriteToLogfile(&$buffer) {
        $instance = self::instance();
        if ($filepath = $instance->getFilepath()) {
            return file_put_contents($filepath, $buffer, FILE_APPEND);
        } else {
            if (fputs(STDERR, $buffer)) {
                return TRUE;
            }
        }
    }

    /**
    * Handle and log information
    * @param  $method The value of the calling __METHOD__
    * @param  $line The value of the calling __LINE__
    * @param  $args The data to debug
    * @return bool
    */
    public static function debug($method, $line, $args = null) {
        $instance = self::instance();
        if ($instance->debugEnabled()) {
            $str  = sprintf('%.4f', microtime(TRUE));
            $str .= ' [debug] ' . $method . ':' . $line;
            if (is_null($args)) {
                // do nothing more
            } elseif (is_scalar($args)) {
                $str .= ' ' . $args;
            } else {
                if (is_array($args)) {
                    foreach ($args as $arg) {
                        if (is_array($arg) || is_object($arg)) {
                            $str .= ' ' . print_r($arg, true);
                        } else {
                            $str .= ' ' . $arg;
                        }
                    }
                } else {
                    $str .= ' ' . print_r((array)$args, true);
                }
            }
            $str .= PHP_EOL;
            return self::doWriteToLogfile($str);
        }
    }
    /**
    * Handle and log information
    * @return bool
    */
    public static function info($message = NULL) {
        $message = is_null($message) ? '' : $message;
        $str = sprintf('%.4f', microtime(1)).' [info] ';
        $str .= rtrim($message);
        $str .= PHP_EOL;
        return self::doWriteToLogfile($str);
    }

    /**
    * Handle and log warnings
    * @param string $message The message to log
    * @return bool
    */
    public static function warning($message) {
        $str = sprintf('%.4f', microtime(1)).' [warning] ';
        $str .= rtrim($message);
        $str .= PHP_EOL;
        return self::doWriteToLogfile($str);
    }

    /**
    * Handle and log errors
    * 
    * You can pass a string to this method, or an exception with a public
    * __toString method which formats the error message for the exception
    * 
    * @param string $message The message to log
    * @return bool
    */
    public static function error($message) {
        $str = sprintf('%.4f', microtime(1)).' [error] ';
        $str .= rtrim($message);
        $str .= PHP_EOL;
        return self::doWriteToLogfile($str);
    }

     /**
     * Is debugging enabled?
     */
    public function debugEnabled() {
        return !!$this->debugEnabled;
    }

    /**
     * Enable debugging
     */
    public function enableDebug() {
        $this->debugEnabled = TRUE;
    }

    /**
     * Disable debugging
     */
    public function disableDebug() {
        $this->debugEnabled = FALSE;
    }

    /**
     * Set debugging logfile filepath
     */
    public function setFilepath($filepath) {

        $logfileAvailable = FALSE;

        if (file_exists($filepath)) {
            if (is_writable($filepath)) {
                $logfileAvailable = TRUE;
            }
        } else {
            if (touch($filepath)) {
                $logfileAvailable = TRUE;
            }
        }

        if (!$logfileAvailable) {
            throw new Exception("Debug logfile not writable: $filepath");
        }

        $this->filepath = $filepath;
    }

    /**
     * Get debugging logfile filepath
     */
    public function getFilepath() {
        return $this->filepath;
    }
}

