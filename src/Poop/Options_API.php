<?php
namespace Poop;
/**
* Interface definition for an Options object
* @package Poop
*/
interface Options_API extends Object_API {

    /**
    * Return the option name
    * @param array $options Optional initial options
    * @return string
    */
    function __construct(array $options = NULL);

    /**
    * Return the number of set options
    * @return bool
    */
    function getCount();

    /**
    * Return true if option exists
    * @param string $name The option name to check
    * @return bool
    */
    function hasOption($name);

    /**
    * Return the Option object
    * @param string $name The option name
    * @return mixed Option object or NULL
    */
    function getOption($name);

    /**
    * Set an new Option object
    * @param string $name The option name
    * @param string $value The option value
    * @return Options
    */  
    function setOption($name, $value);

    /**
    * Return all options
    * @return array An array of Option objects
    */
    function getOptions();
}