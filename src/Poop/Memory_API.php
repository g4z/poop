<?php
namespace Poop;
/**
* Interface definition for a Memory object
* @package Poop
*/
interface Memory_API extends Object_API {

    /**
    * (Re)initialise the memory data
    */
    function reset();

    /**
    * Return the memory data array
    * @return array
    */
    function getData();

    /**
    * Explicitly set the memory data array
    * @param array $data Memory array to set
    * @return Memory
    */
    function setData(array $data);
    
    /**
    * Return true if key variable is sets
    * @param string $key The variable key
    * @return bool
    */
    function hasVar($key);

    /**
    * Get the value for key variable
    * @param string $key The variable key
    * @return mixed The key variable value or NULL
    */
    function getVar($key);
    
    /**
    * Set the value of key variable
    * @param string $key The variable key
    * @param string $value The variable value
    * @param integer $type The variable type (@see Type)
    * @return Memory
    */
    function setVar($key, $value, $type);
}