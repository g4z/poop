<?php
namespace Poop;
/**
* Interface definition for a Engine object
* @package Poop
*/
class Interpreter
extends Engine
implements Interpreter_API
{
    /**
    * @var Environment $environment The Environment object
    * @see Environment
    */
    protected $environment;

    /**
    * @var ShellColour $colourer Object for creating colored output for BASH
    * @see ShellColour
    */
    protected $colourer;

    /**
    * Construct the Interpreter object
    * @param array $options Optional array of name/value options
    */
    public function __construct(array $options = NULL) {
        
        $this->environment = new Environment();
        if (is_null($options)) {
            $options = $this->environment->getOptions();
        }

        $this->colourer = new ShellColour();

        parent::__construct($options);
    }

    /**
    * Return the current memory state array
    * @return array
    */
    public function getState() {
        return $this->memory->getData();
    }

    /**
    * Set the current memory state array
    * @param array $state The array of memory contents
    * @return Interpreter
    */
    public function setState(array $state) {
        $this->memory->setData($state);
        return $this;
    }

    /**
    * Handle the run shell action
    * @return ShellResult
    */
    public function doShell() {

        // $shell is derived from RunResult
        $shell = new Shell();

        while (!$shell->isEOF()) {

            if ($shell->isFail()) {
                break;
            }

            $input = $shell->readInput();
            if ($input === FALSE) {
                break;
            }

            // Available shell commands (for help sreen)
            $shell_commands = array(
                'dump',
                'exit',
                'help',
            );

            // Interpret shell keywords
            switch ($input) {
                case 'exit': 
                    break 2;
                case 'dump';
                    $this->stdout(print_r(
                        $this->getMemory()->getData(), 
                        true
                    ));
                    break;
                case 'help';
                    $extension_manager = $this->getExtensionManager();
                    $exports = $extension_manager->exports();
                    $this->stdout(PHP_EOL);
                    $this->stdout(str_pad(' Commands:', 13) . join(', ', $shell_commands) . PHP_EOL);
                    $this->stdout(str_pad(' Keywords:', 13) . join(', ', $this->getLanguageKeywords()) . PHP_EOL);
                    $this->stdout(str_pad(' Functions:', 13) . join(', ', $exports) . PHP_EOL);
                    $this->stdout(PHP_EOL);
                    break;
                default:
                {
                    // Assume is code
                    $this->setCode($input);

                    $result = $this->doRun();

// TODO: Finished this
// dd($result);
                    // $this->stdout()

                    // Handle printing errors
                    if ($result->isFail()) {
                        foreach ($result->getErrors() as $error) {
                            $this->stderr($this->outputFormatErrorMessage($error));
                        }
                    }

                    break;
                }
            }

            // TODO: Provide interface for setting position :/
            $this->line++;
            $this->column = 0;
        }

        $shell->setData($this->getMemory()->getData());

        return $shell;
    }

    /**
    * Dispatch method for running actions (minimise, run, etc...)
    * @param string $action One of: run|syntaxCheck|tokenise|minimise
    * @return Result Return the result only if in HOSTED mode
    */
    public function doAction($action) {

        $method = 'do' . $action;

        if (!method_exists($this, $method)) {
            $this->error('Invalid action method: ' . $method);
            throw new Exception('Invalid action method: ' . $method);
        }

        $result = $this->$method();

        if ($this->environment->isHosted()) {
            return $result;
        }

        $this->doOutput($result);
    }

    /**
    * Handle output based on Environment and Options
    * @param Result $result Object derived from Result
    * @return Interpreter
    */
    public function doOutput(Result $result) {
        $this->debug(__METHOD__, __LINE__);
        if ($this->environment->isWeb()) {
            $json = json_encode($result->toArray());
            $this->stdout($json);
        } else {
            if ($result->hasErrors()) {
                foreach ($result->getErrors() as $error) {
                    $errstr = $this->outputFormatErrorMessage($error);
                    $this->stderr($errstr);
                }
                if ($result->isFail()) return;
            }
            if (($buffer = trim($result->getOutput()))) {
                $this->stdout($buffer . PHP_EOL);
            }
        }
    }

    private final function outputFormatErrorMessage(array $error) {
        return sprintf('%s [%s] %s%s',
            $this->colourer->getColoredString($error['type'], 'light_red'),
            $this->colourer->getColoredString('' . $error['line'] . '.' . $error['column'], 'light_red'),
            $this->colourer->getColoredString($error['message'], 'cyan'),
            PHP_EOL
        );
    }


    /**
    * Output program usage text
    */
    public function doHelp() {
        $cli = $this->environment->isCli();
        $buffer = sprintf('%s
Usage: %s%s%soption1%s=arg1%s%soption2%s=arg2%s%s ...

Options:

',          $cli ? '' : '<pre>',
            $cli ? basename($GLOBALS['argv'][0]) : basename($_SERVER['SCRIPT_FILENAME']),
            $cli ? ' ' : '?', $cli ? '[' : '', $cli ? '[' : '', $cli ? ']' : '',
            $cli ? '] [' : '&', $cli ? '[' : '', $cli ? ']' : '', $cli ? ']' : ''
        );
        foreach (OptionMap::getInstance() as $option) {
            if (empty($option[2])) {
                $buffer2 = $option[0];
            } else {
                $buffer2 = $option[0] . '=' . mb_strtoupper($option[0]);               
            }
            $buffer .= sprintf(' %s%s %s%-10s %s%s',
                $cli ? '-' : '$',
                $option[1],
                $cli ? '--' : '$',
                $buffer2,
                $option[3],
                PHP_EOL
            );
        }

        $buffer .= sprintf('
%s',        $cli ? '' : '</pre>'
        );

        // Print output
        echo $buffer;


    }
}