<?php
namespace Poop;
/**
* Definition of Shell class
* @package Poop
*/
interface Shell_API extends RunResult_API {
    
    /**
    * Standard input
    */
    const STDIN = STDIN;

    /**
    * Standard output
    */
    const STDOUT = STDOUT;

    /**
    * Standard error
    */
    const STDERR = STDERR;

    /**
    * The default prompt
    */
    const PROMPT = 'poop> ';
    
    /**
    * Read a line from the command prompt
    * @return string
    */
    function readInput();

    /**
    * Return true if we should exit the shell
    * @return bool
    */
    function isEOF();

    /**
    * Output a string
    * @param string $buffer The string to output
    * @return Shell
    */
    function writeOutput($buffer);
}