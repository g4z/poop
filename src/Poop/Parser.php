<?php
namespace Poop;
/**
 * Implement the Parser class
 * @package Poop
 * @uses Memory
 */
class Parser
extends Lexer
implements Parser_API
{
    /**
     * @var Memory $memory
     * @see Memory
     */
    private $memory;

    /**
     * @var ExtensionManager $extensions
     */
    private $extension_manager;

    /**
     * Construct the Parser object
     */
    public function __construct() {
        parent::__construct();
        $this->memory = new Memory();
        $this->extension_manager = new ExtensionManager($this);
    }

    /**
    * Evaluate the next expression
    * @param string &$buffer The expression result value is written here
    * @param Token $token An optional start from token
    * @return int The Type ID of the expression result
    */
    protected function evalExp(&$buffer, $token = NULL)
    {
        static $depth = -1;
        $depth++;
        $this->debug(__METHOD__, __LINE__, 'evalExp depth: ' . $depth);

        $buffer = ''; // write to arg by reference

        if (!$token) {
            $token = $this->getToken();
        }

        if (!$token) {
            throw new SyntaxException(
                $this->getLine(),
                $this->getColumn(),
                'Failed evaluating expression',
                __FILE__, __LINE__
            );
        }

        switch ($token->getType())
        {
            case TokenType::IDENTIFIER:
            {
                $this->debug(__METHOD__, __LINE__, "Evaluating IDENTIFIER token");

                $varname = $token->getValue();

                // If identifier not found in memory
                if (!$this->memory->hasVar($varname)) {
                    throw new SyntaxException(
                        $token->getLine(),
                        $token->getColumn(),
                        "Undefined identifier: '$varname'",
                        __FILE__, __LINE__
                    );
                }

                $variable = $this->memory->getVar($varname);
                $leftside = $variable->getValue();
                $vartype = $variable->getType();

            }
            break;
            case TokenType::MINVOKE:
            {
                $this->debug(__METHOD__, __LINE__, "Evaluating MINVOKE token");
                $result = $this->procMethodInvoke($token);
                $leftside = $result->getValue();
                $vartype = $result->getType();
                unset($result);
            }
            break;
            case TokenType::INTEGER:
            {
                $this->debug(__METHOD__, __LINE__, "Evaluating INTEGER token");
                $vartype = TokenType::INTEGER;
                $leftside = $token->getValue();
            }
            break;
            case TokenType::FLOAT:
            {
                $this->debug(__METHOD__, __LINE__, "Evaluating FLOAT token");
                $vartype = TokenType::FLOAT;
                $leftside = $token->getValue();
            }
            break;
            case TokenType::STRING:
            {
                $this->debug(__METHOD__, __LINE__, "Evaluating STRING token");
                $vartype = TokenType::STRING;
                $leftside = $token->getValue();
            }
            break;
            case TokenType::SYMBOL:
            {
                $this->debug(__METHOD__, __LINE__, "Evaluating SYMBOL token");
                list($leftside, $vartype) = $this->procSymbol($token);
                // $vartype = TokenType::SYMBOL;
                if (!($leftside)) {
                die("O");
                    return;
                }
            }
            break;
            case TokenType::KEYWORD:
            default:
                throw new SyntaxException(
                    $token->getLine(),
                    $token->getColumn(),
                    'Invalid expression: ' . $token->getValue(),
                    __FILE__, __LINE__
                );
            break;

        }
    
        $buffer = $leftside; // assign it incase we exit ;)

        $this->debug(__METHOD__, __LINE__, "Leftside_Value: $buffer");
        // $this->debug(__METHOD__, __LINE__, "Leftside_Type: $vartype");
        $this->debug(__METHOD__, __LINE__, "Leftside_TypeName: " . TokenType::map($vartype));

        // Get the operator part of the expression
        $token = $this->getToken();
        switch ($token->getType()) {
            case TokenType::SYMBOL: 
            {
                switch ($token->getValue()) {
                    case ')':
                        $depth--;
                        $this->ungetCharacter();
                        return $vartype; // hello
                    case ';':
                        $depth--;
                        // $this->ungetCharacter();
                        return $vartype; // hello
                    break;
                    case ',': 
                        return $vartype;
                    break;
                    case '+': // also for string concat
                    case '-':
                    case '*':
                    case '/':
                    case '%':
                        // math operators
                    break;
                    case '==':
                    case '!=':
                    case '<':
                    case '>':
                    case '>=':
                    case '<=':
                        // comparison operators
                    break;
                    case '&&':
                    case '||':
                        // logical
                    break;
                    case '&':
                    case '|':
                        // bitwise
                    break;
                    default:
                    {
                        throw new SyntaxException(
                            $token->getLine(),
                            $token->getColumn(),
                            'Invalid operator: ' . $token->getValue(),
                            __FILE__, __LINE__
                        );
                    }
                    break;
                }
            }
            break;
            default:
            {
                throw new SyntaxException(
                    $token->getLine(),
                    $token->getColumn(),
                    'Invalid symbol: ' . $token->getValue(),
                    __FILE__, __LINE__
                );
            }
            break;
        }

        $operator = $token->getValue();
        
        $this->debug(__METHOD__, __LINE__, 'Operator_Value: ' . $operator);
        // $this->debug(__METHOD__, __LINE__, 'Operator_Type: ' . $token->getType());
        $this->debug(__METHOD__, __LINE__, "Operator_TypeName: " . TokenType::map($token->getType()));
        
        // Recursive call to evaluate the expression further
        $rightside = '';
        $vartype2 = $this->evalExp($rightside);

        $this->debug(__METHOD__, __LINE__, 'Rightside_Value: ' . $rightside);
        // $this->debug(__METHOD__, __LINE__, "Rightside_Type: $vartype2");
        $this->debug(__METHOD__, __LINE__, "Rightside_TypeName: " . TokenType::map($vartype2));

        switch ($vartype) {
    
            case TokenType::FLOAT:
            {
                $n1 = floatval($leftside);
                $n2 = floatval($rightside);                
                
                if ($operator == '+') {
                    $n3 = $n1 + $n2;
                } elseif ($operator == '-') {
                    $n3 = $n1 - $n2;
                } elseif ($operator == '*') {
                    $n3 = $n1 * $n2;
                } elseif ($operator == '/') {
                    $n3 = $n1 / $n2;
                } elseif ($operator == '==') {
                    $n3 = ($n1 == $n2);
                } elseif ($operator == '<') {
                    $n3 = ($n1 < $n2);
                } elseif ($operator == '>') {
                    $n3 = ($n1 > $n2);
                } elseif ($operator == '<=') {
                    $n3 = ($n1 <= $n2);
                } elseif ($operator == '>=') {
                    $n3 = ($n1 >= $n2);
                } elseif ($operator == '!=') {
                    $n3 = ($n1 != $n2);
                } else {
                    throw new SyntaxException(
                        $token->getLine(),
                        $token->getColumn(),
                        'Illegal float operator: ' . $operator,
                        __FILE__, __LINE__
                    );
                }

                // set the output buffer
                $buffer = floatval($n3);
            }
            break;
            case TokenType::INTEGER:
            {
                $n1 = intval($leftside);
                $n2 = intval($rightside);

                if ($operator == '+') {
                    $n3 = $n1 + $n2;
                } elseif ($operator == '-') {
                    $n3 = $n1 - $n2;
                } elseif ($operator == '*') {
                    $n3 = $n1 * $n2;
                } elseif ($operator == '/') {
                    $n3 = $n1 / $n2;
                } elseif ($operator == '%') {
                    $n3 = $n1 % $n2;
                } elseif ($operator == '==') {
                    $n3 = ($n1 == $n2);
                } elseif ($operator == '<') {
                    $n3 = ($n1 < $n2);
                } elseif ($operator == '>') {
                    $n3 = ($n1 > $n2);
                } elseif ($operator == '<=') {
                    $n3 = ($n1 <= $n2);
                } elseif ($operator == '>=') {
                    $n3 = ($n1 >= $n2);
                } elseif ($operator == '!=') {
                    $n3 = ($n1 != $n2);
                } elseif ($operator == '&&') {
                    $n3 = (int)($n1 && $n2);
                } elseif ($operator == '||') {
                    $n3 = (int)($n1 || $n2);
                } elseif ($operator == '&') {
                    $n3 = ($n1 & $n2);
                } elseif ($operator == '|') {
                    $n3 = ($n1 | $n2);
                } else {
                    throw new SyntaxException(
                        $token->getLine(),
                        $token->getColumn(),
                        'Illegal numerical operator: ' . $operator,
                        __FILE__, __LINE__
                    );
                }

                // set the output buffer
                $buffer = intval($n3);
            }
            break;
            case TokenType::STRING:
            {
                $tmp = $rightside;    
                if ($operator == '+') {
                    $buffer .= $rightside;
                } elseif ($operator == '==') {
                    $buffer = ($buffer == $tmp) ? '1' : '0';
                } elseif ($operator == '<') {
                    $buffer = ($buffer < $tmp) ? '1' : '0';
                } elseif ($operator == '>') {
                    $buffer = ($buffer > $tmp) ? '1' : '0';
                } elseif ($operator == '!=') {
                    $buffer = ($buffer != $tmp) ? '1' : '0';
                } elseif ($operator == '<=') {
                    $buffer = ($buffer <= $tmp) ? '1' : '0';
                } elseif ($operator == '>=') {
                    $buffer = ($buffer >= $tmp) ? '1' : '0';
                } elseif ($operator == '&&') {
                    $buffer = ($buffer && $tmp) ? '1' : '0';
                } elseif ($operator == '||') {
                    $buffer = ($buffer || $tmp) ? '1' : '0';
                } elseif ($operator == '&') {
                    $buffer = ($buffer & $tmp) ? '1' : '0';
                } elseif ($operator == '|') {
                    $buffer = ($buffer | $tmp) ? '1' : '0';
                } else {
                    throw new SyntaxException(
                        $token->getLine(),
                        $token->getColumn(),                    
                        'Illegal string operator: ' . $operator,
                        __FILE__, __LINE__
                    );
                }
            }
            break;
            default;
                throw new SyntaxException(
                    $token->getLine(),
                    $token->getColumn(),                    
                    'Unexpected variable type: ' . TokenType::map($vartype),
                    __FILE__, __LINE__
                );
            break;
        }
        
        $this->debug(__METHOD__, __LINE__, "Expression: $leftside $operator $rightside");
        $this->debug(__METHOD__, __LINE__, 'Result: ' . $buffer);
        $this->debug(__METHOD__, __LINE__, 'Type: ' . TokenType::map($vartype));

        $depth--;

        return $vartype;
    }

    /**
     * Parse the next statement block
     * @return ??? Whether the calling code should continue or break;
     */
    protected function parse(/*$depth = 0*/) {
        
        static $depth = -1;
        $depth++;

        $this->debug(__METHOD__, __LINE__, sprintf(
            'Depth(%u) Offset(%s:%s)',
            $depth,
            $this->getLine(),
            $this->getColumn()
        ));

        while ($token = $this->getToken()) {
            switch ($token->getType()) {
                case TokenType::MINVOKE:
                {
                    // Functions calls cannot return a value from here!
                    // list($result, $vartype) = $this->procMethodInvoke($token);
                    // return $result;
                    $this->procMethodInvoke($token);
                }
                break;
                case TokenType::KEYWORD:
                {
                    $retval = $this->procLanguageKeyword($token);
                    if ($retval === FALSE) {
                        break 2;
                    }
                }
                break;
                case TokenType::SYMBOL:
                {
                    $this->procSymbol($token);
                }
                break;
                case TokenType::IDENTIFIER:
                    try {
                        $this->procAssignment($token);
                    } catch (EofException $e) {
                        throw new SyntaxException(
                            $token->getLine(),
                            $token->getColumn(),
                            "Unexpected EOF assigning: '{$token->getValue()}'",
                            __FILE__, __LINE__
                        );
                    }
                break;
                default:
                {
                    $this->debug(__METHOD__, __LINE__, "Unexpected token: {$token->getValue()}");
                    throw new SyntaxException(
                        $token->getLine(),
                        ($token->getColumn() - mb_strlen($token->getValue())) + 1,
                        "Unexpected token: '{$token->getValue()}'",
                        __FILE__, __LINE__
                    );
                }
                break;
            }
        }

        $depth--;
        $this->debug(__METHOD__, __LINE__, 'Parse depth: ' . $depth);

    }

    //======================= Proc Token Types ==========================

    /**
     * Process a symbol token (from parse or evalExpr)
     * @param Token $token The symbol token to start from
     * @return array Array of value and type (eg. [123, Type::INTEGER])
     */
    protected function procSymbol(Token $token) {
        $this->debug(__METHOD__, __LINE__, "Found symbol: {$token->getValue()}");
        switch ($token->getValue()) {
            case ';': 
                $this->debug(__METHOD__, __LINE__, 'Found end-of-statement');
                return array(NULL, $token->getType());
            break;
            case '(':
                $varval = '';
                $vartype = $this->evalExp($varval);
                // proc the rightside close braces
                $token = $this->getToken();
                if ($token->getValue() != ')') {
                    throw new SyntaxException(
                        $token->getLine(),
                        $token->getColumn(),
                        "Expected ')' but got '{$token->getValue()}'",
                        __FILE__, __LINE__
                    );
                }
                return array($varval, $vartype);
            break;
            case '}': // Found end of a statement block
                // $this->ungetCharacter();
                // $this->debug(__METHOD__, __LINE__, 'Leaving parse depth: ' . $depth);
                // if ($depth > 0) $depth--;
                // $this->debug(__METHOD__, __LINE__, 'Changing to parse depth: ' . $depth);
                // return; // Return control to doIf method
                return array(NULL, $token->getType());
            break;
            case '//': // Comment
                $this->debug(__METHOD__, __LINE__, 'Found a comment');
                $comment = '';
                do {
                    $char = $this->getCharacter();
                    $comment .= $char->getValue();
                } while($char->getValue() != "\n");
                $this->debug(__METHOD__, __LINE__, 'Comment: ' . trim($comment));
                return array(trim($comment), TokenType::COMMENT);
            break;
            default:
            {
                throw new SyntaxException(
                    $token->getLine(),
                    $token->getColumn(),
                    'Unexpected symbol: ' . $token->getValue(),
                    __FILE__, __LINE__
                );
            }
        }
    }

    protected function procLanguageKeyword(Token $token) {
        $this->debug(__METHOD__, __LINE__, "Processing: " . $token->getValue());
        try {
            switch ($token->getValue()) {
                case 'quit':
                    $this->procQuit();
                    return FALSE;
                break;
                case 'if':
                    $this->procIf();
                break;
                case 'echo': 
                    $this->procEcho();
                break;
                case 'while':
                    $this->procWhile();
                break;
                case 'break':
                    $token = $this->getToken();
                    if ($token->getValue() != ';') {
                        throw new SyntaxException(
                            $token->getLine(),
                            $token->getColumn(),
                            'Expecting ; but got ' . $token->getValue(),
                            __FILE__, __LINE__
                        );
                    }
                    // Return false to stop parent iteration
                    return FALSE;
                break;
                default:
                {
                    throw new SyntaxException(
                        $token->getLine(),
                        $token->getColumn(),
                        'Unknown keyword: ' . $token->getValue(),
                        __FILE__, __LINE__
                    );
                }
            }
        } catch (EofException $e) {
            // throw new SyntaxException(
            //     $token->getLine(),
            //     $token->getColumn(),
            //     'Unexpected EOF in ' . $token->getValue() . ' block',
            //     __FILE__, __LINE__
            // );                      
        }
    }


    protected function procMethodInvoke(Token $token) {

        $this->debug(__METHOD__, __LINE__, "Found a method call: {$token->getValue()}");

        $method = $token->getValue();

        // Checkout method is available
        if (!$this->extension_manager->resolve($method)) {
            throw new SyntaxException(
                $token->getLine(),
                $token->getColumn(),
                'Call to undefined method: ' . $token->getValue(),
                __FILE__, __LINE__
            );
        }

        // Begin processing ...

        $token = $this->getToken();
        if ($token->getValue() != '(') {
            throw new SyntaxException(
                $token->getLine(),
                $token->getColumn(),
                "Expected '(' but got '{$token->getValue()}'",
                __FILE__, __LINE__
            );
        }

        $arguments = array();

        $token = $this->getToken();
        while ($token->getValue() != ')') {
            $value = NULL;
            switch ($token->getValue()) {
                case ',':
                break;
                default:
                    $vartype = $this->evalExp($value, $token);
                break;
            }

            // $arguments[] = array($value, $vartype);
            $arguments[] = $value;
            $token = $this->getToken();
        }

        return $this->extension_manager->call($method, $arguments);
    }

    // ================== Proc Lang Keywords =======================

    protected function procEcho() {
        $buffer = '';
        $this->evalExp($buffer);
        $this->debug(__METHOD__, __LINE__, 'Output: ' . $buffer . '\n');
        echo $buffer, PHP_EOL;
    }

    protected function procWhile() {
        $this->debug(__METHOD__, __LINE__);
        $token = $this->getToken();
        if ($token->getValue() != '(') {
            throw new SyntaxException(
                $token->getLine(),
                $token->getColumn(),
                'Expecting ( but got: ' . $token->getValue(),
                __FILE__, __LINE__
            );          
        }
        
        // Save current code offset
        $offset = $this->getOffset();

        // Eval the while param
        $buffer = '';
        $type = $this->evalExp($buffer);

        do {
            $token = $this->getToken();
            if ($token->getValue() != ')') {
                throw new SyntaxException(
                    $token->getLine(),
                    $token->getColumn(),
                    'Expecting ) but got: ' . $token->getValue(),
                    __FILE__, __LINE__
                );          
            }
            $token = $this->getToken();
            if ($token->getValue() != '{') {
                throw new SyntaxException(
                    $token->getLine(),
                    $token->getColumn(),
                    'Expecting { but got: ' . $token->getValue(),
                    __FILE__, __LINE__
                );          
            }
            // If the while expression was "false"
            if (!$buffer) {

                $depth = 0;
                do { // Skip over the "while" block
                    $token = $this->getToken();
                    if ($token->getValue() == '{') $depth++;
                    if ($token->getValue() == '}') $depth--;
                } while($token->getValue() != '}' && !$depth);

                break;
            }
            do {
                $retval = $this->parse();
                if ($retval === false) {
                    break 2;
                }
                $token = $this->getToken();
            } while ($token->getValue() != '}');
            
            // dd($this->getToken());
            $this->debug(__METHOD__, __LINE__, "Set offset: $offset");
            // $this->setOffset($offset);
            $this->gotoOffset($offset);
            $this->evalExp($buffer);

        } while($buffer);

        $this->debug(__METHOD__, __LINE__, "Loop COMPLETE");

        // Forward the offset to end of while statement
        $this->debug(__METHOD__, __LINE__, "Move to end of block...");
        do {
            $token = $this->getToken();
        } while ($token->getValue() != '}');
        // $this->ungetCharacter();
        

        // $token = $this->getToken();
        // if ($token->getValue() != '}') {
        //     throw new SyntaxException(
        //         $token->getLine(),
        //         $token->getColumn(),
        //         'Expecting } but got: ' . $token->getValue(),
        //         __FILE__, __LINE__
        //     );          
        // }

        $this->debug(__METHOD__, __LINE__, "Finished");

    }

    protected function procQuit() {
        $this->debug(__METHOD__, __LINE__);
        $token = $this->getToken();
        if ($token->getValue() != ';') {
            throw new SyntaxException(
                $token->getLine(),
                $token->getColumn(),
                'Expected ; but got ' . $token->getValue(),
                __FILE__, __LINE__
            );
        }
        $this->debug(__METHOD__, __LINE__, 'Goodbye!');
    }

    protected function procIf() {

        $this->debug(__METHOD__, __LINE__);

        // keep track of recursion depth
        static $depth = 0;
        // $this->debug(__METHOD__, __LINE__, 'Initial IF depth: ' . $depth);

        $depth++;
        
        $this->debug(__METHOD__, __LINE__, 'IF depth: ' . $depth);

        $retval = null;

        $this->debug(__METHOD__, __LINE__, 'Expecting: (');
        $token = $this->getToken();
        if ($token->getValue() != '(') {
            throw new SyntaxException(
                $token->getLine(),
                $token->getColumn(),
                'Expected ( but got ' . $token->getValue(),
                __FILE__, __LINE__
            );
        }

        // Evaluation if() expression
        $result = '';
        $this->debug(__METHOD__, __LINE__, 'Evaluating IF statement...');
        $vartype = $this->evalExp($result);
        $this->debug(__METHOD__, __LINE__, 'EVAL_RESULT: ' . $result);

        $this->debug(__METHOD__, __LINE__, 'Expecting: )');
        $token = $this->getToken();
        if ($token->getValue() != ')') {
            throw new SyntaxException(
                $token->getLine(),
                $token->getColumn(),
                'Expected ) but got ' . $token->getValue(),
                __FILE__, __LINE__
            );
        }

        $this->debug(__METHOD__, __LINE__, 'Expecting: {');
        $token = $this->getToken();
        if ($token->getValue() != '{') {
            throw new SyntaxException(
                $token->getLine(),
                $token->getColumn(),
                'Expected { but got ' . $token->getValue(),
                __FILE__, __LINE__
            );
        }

        // Process statement block if evaluation was TRUE
        if ($result) {

            $this->debug(__METHOD__, __LINE__, 'IF_EVAL_RESULT = TRUE');
            $this->debug(__METHOD__, __LINE__, 'Processing IF statement block...');
            $retval = $this->parse();
            $this->debug(__METHOD__, __LINE__, 'Expecting: }');
            $token = $this->getToken();
            if ($token->getValue() != '}') {
                throw new SyntaxException(
                    $token->getLine(),
                    $token->getColumn(),
                    'Expected } but got ' . $token->getValue(),
                    __FILE__, __LINE__
                );
            }

            // TODO: Fixme here in IF functionality
            // $token = $this->getToken();

            $this->debug(__METHOD__, __LINE__, 'IF statement block processed');
            $this->debug(__METHOD__, __LINE__, 'Leaving IF depth: ' . $depth);

            // try {
            //     $token = $this->getToken();
            // } catch (EofException $e) {
            // }

            $depth--;
            $this->debug(__METHOD__, __LINE__, 'Changing to IF depth: ' . $depth);
            $this->debug(__METHOD__, __LINE__, 'Current offset: ' . $this->getLine() . ':' . $this->getColumn());

            return $retval;

        } else { // Skip over the if block
            $this->debug(__METHOD__, __LINE__, 'IF eval was FALSE');
            $this->debug(__METHOD__, __LINE__, 'Ignoring IF statement block');

            $olddepth = $depth;

            do {

                $this->debug(__METHOD__, __LINE__, '-------------------');
                $this->debug(__METHOD__, __LINE__, 'Current offset: ' . $this->getLine() . ':' . $this->getColumn());
                $this->debug(__METHOD__, __LINE__, '-------------------');

                try {
                    // $token = $this->getToken();
                    $char = $this->getCharacter();
                    if ($char->getValue() == '{') $olddepth++;
                    if ($char->getValue() == '}') $olddepth--;
                } catch (EofException $e) {
                    $depth--;
                    // return $retval;
                }
            } while($char->getValue() != '}' && ($olddepth >= $depth));
        }
        
        // Test if there's an else statement
        try {
            $token = $this->getToken();
        } catch (EofException $e) {
            $depth--;
            return $retval;
        }
        if ($token->getValue() != 'else') {
            $depth--;
            $this->debug(__METHOD__, __LINE__, 'Changing to IF depth: ' . $depth);
            return $retval;
        }
        
        // $this->debug(__METHOD__, __LINE__, 'Ignoring IF statement block');

        // "Else" block starting brace
        $token = $this->getToken();
        if ($token->getValue() != '{') {
            throw new SyntaxException(
                $token->getLine(),
                $token->getColumn(),
                'Expected { but got ' . $token->getValue(),
                __FILE__, __LINE__
            );
        }

        if (!$result) { // If we didn't run the if block

            // Parse the "else" block
            $retval = $this->parse();

            // "Else" block ending brace
            $token = $this->getToken();
            if ($token->getValue() != '}') {
                throw new SyntaxException(
                    $token->getLine(),
                    $token->getColumn(),
                    'Expected } but got ' . $token->getValue(),
                    __FILE__, __LINE__
                );
            }

        } else {

            do { // Skip over the "else" block
                $token = $this->getToken();
                if ($token->getValue() == '{') $depth++;
                if ($token->getValue() == '}') $depth--;
            } while($token->getValue() != '}' && !$depth);
        }

        $depth--;
        return $retval;
    }

    // ================= Proc Various ======================

    protected function procAssignment(Token $token) {
        $this->debug(__METHOD__, __LINE__, "identifier: {$token->getValue()}");
        // Validate the leftside
        if ($token->getType() != TokenType::IDENTIFIER) {
            throw new SyntaxException(
                $token->getLine(),
                ($token->getColumn() - mb_strlen($token->getValue())) + 1,
                "Invalid assignment identifier: '{$token->getValue()}'",
                __FILE__, __LINE__
            );          
        }
        $name = $token->getValue();

        $token = $this->getToken();

        // Validate the operator
        switch ($token->getType()) {
            case TokenType::SYMBOL:
                switch ($token->getValue()) {
                    case ';':
                        if (!$this->memory->hasVar($name)) {
                            throw new SyntaxException(
                                $token->getLine(),
                                ($token->getColumn() - mb_strlen($name)) + 1,
                                "Unknown identifier: '$name'",
                                __FILE__, __LINE__
                            );
                        }
                        // Do nothing :\
                        return;
                    break;
                    case '=':
                        // allow =
                    break;
                    case '++':
                    case '--':
                        // allow ++ and --
                    break;
                    default:
                    {
                        throw new SyntaxException(
                            $token->getLine(),
                            ($token->getColumn() - mb_strlen($token->getValue())) + 1,
                            "Invalid assignment operator: '{$token->getValue()}'",
                            __FILE__, __LINE__
                        );
                    }
                }
            break;
            default:
            {
                throw new SyntaxException(
                    $token->getLine(),
                    ($token->getColumn() - mb_strlen($token->getValue())) + 1,
                    "Invalid assignment operator: '{$token->getValue()}'",
                    __FILE__, __LINE__
                );
            }
        }
        $operator = $token->getValue();

        // Evaluate the rightside
        $type = $this->evalExp($value);

        // Perform action
        switch ($operator) {
            case '=':
                $this->memory->setVar($name, $value, $type);
            break;
            case '++': // number only
            case '--':
                if (!$this->memory->hasVar($name)) {
                    throw new SyntaxException(
                        $token->getLine(),
                        $token->getColumn(),
                        "Unknown identifier: '$name'",
                        __FILE__, __LINE__
                    );
                }
                $value = $this->memory->getVar($name);
                if ($operator == '++') {
                    $newval = intval($value + 1);
                } else {
                    $newval = intval($value - 1);
                }
                $this->memory->setVar($name, $newval, $type);
            break;
            default:
                throw new SyntaxException(
                    $token->getLine(),
                    ($token->getColumn() - mb_strlen($operator)) + 1,
                    "Unknown operator: '$operator'",
                    __FILE__, __LINE__
                );
            break;
        }

    }

    //====================== getter/setter =========================

    /**
    * Return the reference to the Memory object
    * @return Memory
    */
    public function getMemory() { 
        return $this->memory;
    }

    /**
    * Return the reference to the ExtensionManager object
    * @return ExtensionManager
    */
    protected function getExtensionManager() {
        return $this->extension_manager;
    }
}
