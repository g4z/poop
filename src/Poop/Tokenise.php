<?php
namespace Poop;
/**
* Implementation of Tokenise
* @package Poop
*/
class Tokenise
extends Interpreter
implements Tokenise_API
{
    /**
    * Construct an instance
    * @param array $options Optional array of name/value options
    * @see Options
    */
    public function __construct(array $options = NULL) {
        parent::__construct($options);
    }

    /**
    * Run this action and return a result object
    * @return TokeniseResult
    */
    public function run() {
        return $this->doAction('Tokenise');
    }
}