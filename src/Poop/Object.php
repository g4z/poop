<?php
namespace Poop;
/**
 * Base object for all other objects
 * @package Poop
 */
class Object 
implements Object_API
{
    // *
    //  * Debug flag (default: disabled)
    //  * @see self::enableDebug()
    //  * @see self::disableDebug()
     
    // private $debugEnabled = FALSE;

    // /**
    //  * Path to the debugging logfile
    //  */
    // private $debugFilepath;

    /**
    * Return a string representation of something and exit (debug)
    * @param mixed $data Data to dump
    * @return string A string representation of this class
    */
    public function repr($data = NULL) {
        if ($data) {
            if (is_object($data) || is_array($data)) {
                return print_r($data, TRUE);
            } else {
                return $data;
            }
        } else {
            return print_r($this, TRUE);
        }
    }

    /**
    * Print a string representation of something and exit (debug)
    * @param mixed $data Data to dump
    */
    public function dump($data = NULL) {
        if ($data) {
            if ($data instanceof Object) {
                echo $data->repr(), PHP_EOL;
            } else {
                echo $this->repr($data), PHP_EOL;
            }
        } else {
            echo $this->repr();
        }
        exit(1);
    }

    /**
    * List methods available in object (debug)
    */
    public function help() {
        return sprintf('%s: %s', get_class($this),
            print_r(get_class_methods($this), TRUE)
        );
    }

    /**
    * Global debugging output method (debug)
    * @param  $method The value of the calling __METHOD__
    * @param  $line The value of the calling __LINE__
    * @param  $args The data to debug
    */
    public function debug($method, $line, $args = NULL) {
        Log::debug($method, $line, $args);
    }

    /**
    * Global info logging
    * @param string $message The message to output to the logfile
    */
    public function info($message = NULL) {
        Log::info($message);
    }

    /**
    * Global warning logging
    * @param string $message The message to output to the logfile
    */
    public function warning($message) {
        Log::warning($message);
    }

    /**
    * Global error logging
    * @param string $message The message to output to the logfile
    */
    public function error($message) {
        Log::error($message);
    }

}
