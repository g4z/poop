<?php
namespace Poop;
/**
* Implement the OptionMap class
* @package Poop
*/
class OptionMap
extends Object
implements Iterator, OptionMap_API
{
    /**
    * Prevert external instantiation
    */
    protected function __construct() {}

    /**
    * Return a singleton instance
    * @return OptionMap
    */
    public static function getInstance() {
        static $instance = NULL;
        if (is_null($instance)) {
            $instance = new self;
        }
        return $instance;
    }

    /**
    * @var array $options Array of available options data
    */
    protected $options = array(

//          optname    opt   arg?   desc for --help
'c' => array('code',    'c', TRUE,  'Source code string'),
'd' => array('debug',   'd', FALSE, 'Enable debugging'),
'f' => array('file',    'f', TRUE,  'Path to code file'),
'h' => array('help',    'h', FALSE, 'Display program help'),
'g' => array('gui',     'g', FALSE, 'Display the web GUI'),
'm' => array('minimise','m', FALSE, 'Output minimised code'),
's' => array('syntax',  's', FALSE, 'Perform a syntax check'),
't' => array('tokenise','t', FALSE, 'Output tokenised code')
    
    );

    /**
    * Return current value (option array)
    * @see Iterator
    * @return array
    */
    public function current() { return current($this->options); }
    
    /**
    * Return current key (getopt shortname)
    * @see Iterator
    * @return string
    */
    public function key() { return key($this->options); }

    /**
    * Move to the next index
    * @see Iterator
    */
    public function next() { next($this->options); }

    /**
    * Rewind to the first index
    * @see Iterator
    */
    public function rewind() { reset($this->options); }

    /**
    * Check if the current index is valid
    * @see Iterator
    * @return bool
    */
    public function valid() { 
        return !(FALSE === current($this->options)); 
    }
}