<?php
namespace Poop;
/**
* Implementation of engine result class
* @package Poop
*/
abstract class Result
extends Object
implements Result_API
{
    /**
    * @var int $status Stores the result status
    * @see Result_API::OK
    * @see Result_API::FAIL
    */
    protected $status;

    /**
    * @var array $data Internal data buffer/array
    */
    protected $data;

    /**
    * @var string $output The output buffer (STDOUT)
    */
    protected $output;

    /**
    * @var array $error List of error messages
    */
    protected $errors;

    /**
    * Construct the result object
    */
    public function __construct() {
        $this->status = self::OK;
        $this->data = array();
        $this->output = '';
        $this->errors = array();
    }

    /**
    * Return an array representation
    * @overide
    * @return array
    */  
    public function toArray() {
        return array(
            'status' => $this->status,
            'type' => get_class($this),
            'data' => $this->data,
            'output' => $this->output,
            'errors' => $this->errors
        );
    }

    /**
    * Check if status is OK
    * @return bool
    */
    public function isOk() { return ($this->status == self::OK); }

    /**
    * Check if status is FAIL
    * @return bool
    */
    public function isFail() { return ($this->status == self::FAIL); }

    /**
    * Set the result status
    * @param int $status One of the class constant status values
    * @return Result
    */
    public function setStatus($status) { $this->status = $status; }
    
    /**
    * Get the result status value
    * @return int
    */
    public function getStatus() { return $this->status; }

    /**
    * Explicitly set the internal data array
    * @param array $data The data array to set
    * @return Result
    */
    public function setData($data) { 
        $this->data = $data;
        return $this;
    }

    /**
    * Return the internal data array
    * @return array
    */
    public function getData() { return $this->data; }

    /**
    * Add a record to the internal data array
    * @param mixed $data
    * @return Result
    */
    public function addData($data) { 
        $this->data[] = $data;
        return $this;
    }

    /**
    * Clear/reset the internal data array
    * @return Result
    */
    public function clearData() { 
        $this->data = array();
        return $this;
    }

    /**
    * Return the output buffer
    * @return string
    */
    public function getOutput() { return $this->output; }

    /**
    * Set the output buffer string
    * @param string $text The string value to set
    * @return RunResult
    */
    public function setOutput($buffer) {
        $this->output = $buffer;
        return $this;
    }

    /**
    * Append text to the output buffer
    * @param string $text The string value to append
    * @return RunResult
    */
    public function addOutput($buffer) {
        $this->output .= $buffer;
        return $this;
    }

    /**
    * Clear the output buffer
    * @return RunResult
    */
    public function clearOutput() {
        $this->output = '';
        return $this;
    }

    /**
    * Add a record to the internal error array
    * @param Exception $e An exception object
    * @return Result
    */
    public function addError(Exception $e) {
        $type = str_replace(__NAMESPACE__ . '\\', '', get_class($e));
        $error = array(
            'type' => $type,
            'message' => $e->getMessage()
        );
        switch ($type) {
            case 'SyntaxException':
                $error['line'] = $e->getLineNumber();
                $error['column'] = $e->getColumnNumber();
            break;
            default:
                $error['line'] = $e->getLine();
                $error['column'] = 0;
            break;
        }
        $this->errors[] = $error;
        return $this;
    }

    /**
    * Return true if whtere are errors
    * @return bool
    */
    function hasErrors() { return (count($this->errors) > 0); }

    /**
    * Return the internal error array
    * @return array
    */
    function getErrors() { return $this->errors; }

    /**
    * Clear/reset the internal error array
    * @return Result
    */
    function clearErrors() {
        $this->errors = array();
        return $this;
    }
}