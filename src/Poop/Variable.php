<?php
namespace Poop;
/**
* Class that implements a variable
* @package Poop
*/
class Variable
extends Object
// implements Variable_API
{
    /**
    * @var int $type Stores the integer value for the type of the variable
    * @see Type
    */  
    public $type;

    /**
    * @var mixed $value Stores the value of the variable
    */  
    public $value;

    /**
     * Construct the Variable object
     * @param string $value The variable's value
     * @param integer $type Integer value of the type (@see Type)
     */
    public function __construct($value, $type) {
        $this->value = $value;
        $this->type = $type;
    }

    /**
    * Return the type of variable
    * @return integer The string repr of the type
    */
    public function getType() {
        return $this->type;
    }

    /**
    * Return the variable type name
    * @return string The value of the type (@see Type::map)
    */
    public function getTypeName() {
        return Type::map($this->type);
    }

    /**
    * Return the variables value
    * @return mixed
    */
    public function getValue() { 
        return $this->value;
    }

}
