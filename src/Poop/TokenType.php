<?php
namespace Poop;
/**
 * Language type functionality
 * @package Poop
 * @static
 */
class TokenType
extends Type 
implements TokenType_API
{
    /**
    * Map token type ID to string
    */  
    protected static $tokenmap = array(
        1024    => 'SYMBOL',
        2048    => 'IDENTIFIER',
        4096    => 'KEYWORD',
        8192    => 'MINVOKE',
        16384   => 'COMMENT',
    );

    /**
    * Overide the map method with support for Token types
    * @param int $type Integer representation of a type
    */
    public static function map($type) {
        if (isset(self::$tokenmap[$type])) {
            return self::$tokenmap[$type];
        }
        if (isset(parent::$map[$type])) {
            return parent::$map[$type];
        }
        throw new Exception('Unknown type ID: ' . $type);
    }
}
