<?php
namespace Poop;
/**
* Interface definition for a Lexer object
* @package Poop
*/
interface Lexer_API 
extends Reader_API
{
    /**
    * Get the current line number
    * @return int
    */
    function getLine();

    /**
    * Get the current column number
    * @return int
    */
    function getColumn();

    /**
    * Return the ord value of the next character
    * @return int
    */
    function getCharacter();

    /**
    * Push the past character back to the buffer
    * @return int
    */
    function ungetCharacter();

    /**
    * Return the next token from the buffer
    * @return Token
    */
    function getToken();    
}