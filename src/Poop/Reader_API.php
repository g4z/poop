<?php
namespace Poop;
/**
* Interface definition for a Reader object
* @package Poop
*/
interface Reader_API extends Object_API {

    /**
    * Return the character code of the current buffer offset
    * @throws EofException
    * @return int
    */
    function getOrd();

    /**
    * Set the internal code buffer string
    * @param string $buffer The string containing the source code
    * @return Parser
    */
    function setBuffer($buffer);

    /**
    * Get the internal code buffer string
    * @return string The source code from the internal buffer
    */
    function getBuffer();

    /**
    * Set the internal buffer offset
    * @param int $offset The new offset number
    * @return Parser
    */
    function setOffset($offset);

    /**
    * Get the internal buffer offset
    * @return int
    */
    function getOffset();
}