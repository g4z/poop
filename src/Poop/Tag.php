<?php
namespace Poop;
/**
 * Base class for objects emitted by the Lexer
 * @package Poop
 * @abstract
 */
abstract class Tag
extends Object
implements Tag_API
{
    /**
    * @var string The textual content of the tag
    */
    protected $value;

    /**
    * @var int The line number of the tag
    */
    protected $line;

    /**
    * @var int The column number of the tag
    */
    protected $column;
    
    /**
    * Construct this Tag object
    * @param int $line The line number of the tag
    * @param int $column The column number of the tag
    */
    public function __construct($line, $column) {
        $this->value = '';
        $this->line = $line;
        $this->column = $column;
        // $this->debug(
        //     __METHOD__,
        //     __LINE__,
        //     sprintf(
        //         'New %s at offset %u:%u',
        //         mb_strtoupper(get_class($this)),
        //         $this->getLine(),
        //         $this->getColumn()
        //     )
        // );
    }

    /**
    * Return the line number
    * @return int The line number of the tag
    */
    public function getLine() {
        return $this->line;
    }

    /**
    * Return the column number
    * @return int The column number of the tag
    */
    public function getColumn() {
        return $this->column;
    }

    /**
    * Return text value
    * @return string
    * @abstract
    */
    public function getValue() {
        return $this->value;
    }

    /**
    * Set the text value
    * @param string $value The value to set
    * @return Tag
    * @abstract
    */
    //abstract public function setValue($value);

    /**
    * Return an array representation of this object
    * @return array
    * @abstract
    */
    //abstract public function toArray();

    /**
    * Test value empty
    * @return bool
    */
    public function isEmpty() { return empty($this->line); }

    /**
    * Test value alpha-numeric
    * @return bool
    */
    public function isAlnum() { return ctype_alnum($this->value); }

    /**
    * Test value alphabetic
    * @return bool
    */    
    public function isAlpha() { return ctype_alpha($this->value); }

    /**
    * Test value is control character
    * @return bool
    */    
    public function isCtrl() { return ctype_cntrl($this->value); }

    /**
    * Test value numeric
    * @return bool
    */        
    public function isNumber() { return ctype_digit($this->value); }

    /**
    * Test value is printable character
    * @return bool
    */            
    public function isPrint(){ return ctype_print($this->value); }

    /**
    * Test value is printable character but not whitespace
    * @return bool
    */            
    public function isGraph() { return ctype_graph($this->value); }

    /**
    * Test value is lowercase
    * @return bool
    */            
    public function isLower() { return ctype_lower($this->value); }

    /**
    * Test value is uppercase
    * @return bool
    */            
    public function isUpper() { return ctype_upper($this->value); }

    /**
    * Test value is punctuation
    * @return bool
    */                
    public function isPunct() { return ctype_punct($this->value); }
    
    /**
    * Test value is whitespace
    * @return bool
    */       
    public function isSpace() { return ctype_space($this->value); }

    /**
    * Test value is a hex value
    * @return bool
    */           
    public function isHex() { return ctype_xdigit($this->value); }
}