<?php
namespace Poop;

use Poop\TokenType;

/**
 * Class to represent a language token
 * @package Poop
 */
class Token 
extends Tag 
implements Token_API
{
    /**
    * @var int $type Type ID of this token
    * @see Type
    */
    protected $type;

    /**
    * @var int $type Type ID of this token
    * @see Type
    */  
    /*public    function __construct($line, $col) {
        $this->line = $line;
        $this->col = $col;
    }*/

    /**
    * Return the type name for this token
    * @return string
    */
    public function getTypeName() {
        return TokenType::map($this->type);
    }

    /**
    * Return the type ID for this token
    * @return int
    */
    public function getType() {
        return $this->type;
    }
 
    /**
    * Set the token type ID
    * @param int $type The type ID value
    * @see Type
    * @return Token
    */
    public function setType($type) {
        $this->type = $type;
        return $this;
    }

    /**
    * Implement abstract Tag::setValue()
    * @param string $value The token string value
    * @return Token
    */
    public function setValue($value) {
        $this->value = $value;
        return $this;
    }

    /**
    * Implement abstract Tag::toArray()
    * @return array
    */
    public function toArray() {
        return array(
            'value'     => $this->value,
            'line'      => $this->line,
            'column'    => $this->column,
            'typeid'    => $this->type,
            'type'      => TokenType::map($this->type)
        );
    }

    /**
     * 
     */
    public function contains($buffer) {
        return (FALSE !== strstr($this->getValue(), $buffer));
    }
}
