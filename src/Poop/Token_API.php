<?php
namespace Poop;
/**
* Interface definition for a Token object
* @package Poop
*/
interface Token_API extends Tag_API {
    
    /**
    * Return the token type ID (@see Type)
    * @return int
    */
    function getType();

    /**
    * Set the token type ID
    * @param int $type The type ID value
    * @see Type
    * @return Token
    */
    function setType($type);

    /**
    * Return the type name for this token
    * @return string
    */
    function getTypeName();
}
