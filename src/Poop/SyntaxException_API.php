<?php
namespace Poop;
/**
* Interface definition for a Dumpable object
* @package Poop
*/
interface SyntaxException_API {
   
    /**
    * Return the error's source code line number
    * @return mixed
    */
    function getLineNumber();

    /**
    * Return the error's source code column number
    * @return mixed
    */
    function getColumnNumber();

    /**
    * Return the internal (PHP) filename of the error
    * @return mixed
    */
    function getInternalFilename();

    /**
    * Return the internal error line number
    * @return mixed
    */
    function getInternalLineNumber();
}