<?php
namespace Poop;
/**
* Interface definition for a Character object
* @package Poop
*/
interface Character_API extends Tag_API {
    
    /**
    * Return TRUE if character is EOL
    * @return bool
    */
    function isEOL();

    /**
    * Set value by character code (ord)
    * @param int $ord A character code
    * @return Character
    */
    function setOrd($ord);

    /**
    * Return the ord value of the character
    * @return int
    */
    function toOrd();

    /**
    * Return the hexidecimal ord value
    * @return string
    */
    function toHex();   
}