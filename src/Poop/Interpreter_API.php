<?php
namespace Poop;
/**
* Interface definition for a Engine object
* @package Poop
*/
interface Interpreter_API
extends Engine_API
{
    /**
    * Return the current memory state array
    * @return array
    */
    function getState();

    /**
    * Set the current memory state array
    * @param array $state The array of memory contents
    * @return Interpreter
    */
    function setState(array $state);

    /**
    * Handle the run shell action
    * @return ShellResult
    */
    function doShell();

    /**
    * Dispatch method for running actions (minimise, run, etc...)
    * @param string $action One of: run|syntaxCheck|tokenise|minimise
    * @return Result Return the result only if in HOSTED mode
    */
    function doAction($action);

    /**
    * Handle output based on Environment and Options
    * @param Result $result Object derived from Result
    * @return Interpreter
    */
    function doOutput(Result $result);

    /**
    * Output program usage text
    */
    function doHelp();
}