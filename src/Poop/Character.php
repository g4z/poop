<?php
namespace Poop;
/**
 * Class to represent a single character
 * @package Poop
 */
class Character
extends Tag
implements Character_API
{
    /**
    * Implement abstract Tag::setValue()
    * @param string $value A single character
    * @return Character
    */
    public function setValue($value) {
        $this->value = $value;
        return $this;
    }

    /**
    * Set value by character code (ord)
    * @param int $ord A character code
    * @return Character
    */
    public function setOrd($ord) {
        $this->value = chr($ord);
        return $this;
    }

    /**
    * Implement abstract Tag::toArray()
    * @return array
    */
    public function toArray() {
        return array(
            'value'     => $this->value,
            'line'      => $this->line,
            'column'    => $this->column
        );
    }

    /**
    * Return the character code (ord value)
    * @return int
    */
    public function toOrd() {
        return ord($this->value);
    }

    /**
    * Return the character code as hexidecimal
    * @return string
    */
    public function toHex() {
        return dechex(ord($this->value));
    }

    /**
    * Return the character code as hexidecimal
    * @return string
    */
    public function isEOL() {
        return (ord($this->value) == 10);
    }
}