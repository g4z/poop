<?php
namespace Poop;
/**
 * Language type functionality
 * @package Poop
 * @static
 */
class Type
extends Object 
implements Type_API
{
    /**
    * Map token type ID to string
    */  
    protected static $map = array(
        0 => 'NULL',
        1 => 'BOOLEAN',
        2 => 'STRING',
        4 => 'INTEGER',
        8 => 'FLOAT',
        16 => 'COLLECTION',
        32 => 'OBJECT',
        64 => 'RESOURCE',
    );

    /**
    * Return the name for a type ID
    * @param int $type One of the constants from Type_API
    * @throws Exception
    */
    public static function map($type) {
        // dd(debug_backtrace());
        if (!isset(self::$map[$type])) {
            throw new Exception('Unknown type ID: ' . $type);
        }
        return self::$map[$type];
    }

    /**
    * Map a PHP data type to ours
    * @param mixed $value Any type of data
    * @throws Exception
    */
    public static function convert($type) {
        switch (getType($type)) {
            case 'NULL':
                return Type::NULL;
            break;
            case 'boolean':
                return Type::BOOLEAN;
            break;
            case 'integer':
                return Type::INTEGER;
            break;
            case 'double':
                return Type::FLOAT;
            break;
            case 'string':
                return Type::STRING;
            break;
            case 'array':
                return Type::COLLECTION;
            break;
            case 'object':
                return Type::OBJECT;
            break;
            case 'resource':
                return Type::RESOURCE;
            break;
            case 'unknown type':
                return Type::UNKNOWN;
            break;
        }
    }

}
