<?php
namespace Poop;
/**
* Class that implements a variable
* @package Poop
*/
class MemoryVariable
extends Variable
// implements MemoryVariable_API
{
    /**
    * @var string $key Stores the variable name
    */  
    public $key;

    /**
     * Construct the Variable object
     * @param string $key The variable name
     * @param string $value The variable's value
     * @param integer $type Integer value of the type (@see Type)
     */
    public function __construct($key, $value, $type) {
        parent::__construct($value, $type);
        $this->key = $key;
    }

    /**
    * Return the name of variable
    * @return string The name of the variable
    */
    public function getKey() {
        return $this->key;
    }

}
