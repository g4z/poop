<?php
namespace Poop;
/**
* Definition of Dispatcher_API
* @package Poop
*/
interface Dispatcher_API extends Interpreter_API {
    /**
    * Run the dispatcher object
    * @return int The integer retval (0 = OK, !0 = ERR)
    */
    function run();
}