<?php
namespace Poop;
/**
* Interface definition for a Type object
* @package Poop
*/
interface Type_API {

    /**
    * Represents the NULL type
    */  
    const NULL = 0;

    /**
    * Represents the BOOLEAN type
    */  
    const BOOLEAN = 1;

    /**
    * Represents the STRING type
    */  
    const STRING = 2;

    /**
    * Represents the INTEGER type
    */  
    const INTEGER = 4;

    /**
    * Represents the FLOAT type
    */  
    const FLOAT = 8;

    /**
    * Represents the ARRAY type
    */  
    const COLLECTION = 16;

    /**
    * Represents the OBJECT type
    */  
    const OBJECT = 32;

    /**
    * Represents the RESOURCE type
    */  
    const RESOURCE = 64;

    /**
    * Map a type constant to a string value
    * @param int $type One of the Type_API::XXX type constants
    * @return string The type name string
    */
    static function map($type);
}
