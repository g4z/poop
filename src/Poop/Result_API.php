<?php
namespace Poop;
/**
* Definition of engine result class
* @package Poop
*/
interface Result_API extends Object_API {

    /**
    * Represents a successful exit state
    */
    const OK = 'OK';

    /**
    * Represents a failure exit state
    */
    const FAIL = 'FAIL';

    /**
    * Return an array representation
    * @return array
    */  
    function toArray();

    /**
    * Set the result status value
    * @param int $status One of the class constant status values
    * @return Result
    */
    function setStatus($status);

    /**
    * Get the result status value
    * @return int
    */
    function getStatus();

    /**
    * Check if status is OK
    * @return bool
    */
    function isOk();

    /**
    * Check if status is FAIL
    * @return bool
    */
    function isFail();
    
    /**
    * Explicitly set the internal data array
    * @param array $data The data array to set
    * @return Result
    */
    function setData($data);
    
    /**
    * Return the internal data array
    * @return array
    */
    function getData();

    /**
    * Add a record to the internal data array
    * @param mixed $data
    * @return Result
    */
    function addData($data);

    /**
    * Clear/reset the internal data array
    * @return Result
    */
    function clearData();

    /**
    * Set the output buffer string
    * @param string $buffer The string value to set
    * @return RunResult
    */
    function setOutput($buffer);

    /**
    * Return the output buffer
    * @return string
    */
    function getOutput();

    /**
    * Append text to the output buffer
    * @param string $text The string value to append
    * @return RunResult
    */
    function addOutput($buffer);

    /**
    * Clear the output buffer
    * @return RunResult
    */
    function clearOutput();

    /**
    * Add a record to the internal error array
    * @param Exception $e The exception object
    * @return Result
    */
    function addError(Exception $e);

    /**
    * Return true if whtere are errors
    * @return bool
    */
    function hasErrors();

    /**
    * Return the internal error array
    * @return array
    */
    function getErrors();

    /**
    * Clear/reset the internal error array
    * @return Result
    */
    function clearErrors();
}