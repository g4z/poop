<?php
namespace Poop;
/**
* Class to convert source code to tokens
* @package Poop
*/
abstract class Lexer
extends Reader
implements Lexer_API
{
    /**
    * @var int $line The current line number (start at 1)
    */
    protected $line;

    /**
    * @var int $line The current column number (start at 1)
    */
    protected $column;

    /**
    * @var array $keywords Defined language keywords
    */
    private $keywords = array(
        'break',
        'echo',
        'if',
        'quit',
        'while',
    );

    /**
    * Construct the Lexer object
    */
    public function __construct() {
        parent::__construct();
        $this->line = 1;
        $this->column = 1;
    }

    /**
     * This method wraps the setOffset method and handles line/column pointers
     */
    public function gotoOffset($offset) {
        $currentOffset = $this->getOffset();
        // echo "CurrentLine: ", $this->getLine(), PHP_EOL;
        // echo "CurrentOffset: ", $currentOffset, PHP_EOL;
        // echo "RequiredOffset: ", $offset, PHP_EOL;
        if ($currentOffset == $offset) {
            return;
        } elseif ($currentOffset < $offset) {
            $buffer = mb_substr($this->getBuffer(), $currentOffset, $offset);
            // echo "Buffer: ", $buffer, PHP_EOL;
            $this->line += (count(explode("\n", $buffer)) - 1);
        } else {
            $buffer = mb_substr($this->getBuffer(), $offset, $currentOffset);
            // echo "Buffer: ", $buffer, PHP_EOL;
            $this->line -= (count(explode("\n", $buffer)) - 1);
        }
        // TODO: Fix the column pointer on setOffset
        // $this->column += count(explode("\n", $buffer) - 1);
        $this->setOffset($offset);
    }

    /**
    * Return the current source code line number
    * @return int The line number
    */
    public function getLine() { return $this->line; }
    
    /**
    * Return the current source code column number
    * @return int The column number
    */
    public function getColumn() { return $this->column; }

    /**
    * Return the character from the current offset
    * @return string A single character
    */
    public function getCharacter() {
        $ord = $this->getOrd();
        $char = new Character($this->getLine(), $this->getColumn());
        $char->setOrd($ord);

        // TODO: Remove this debug
        if (!ctype_print($char->getValue()) || ctype_space($char->getValue())) {
            $buffer = '0x' . str_pad(mb_strtoupper($char->toHex()), 2, '0',  STR_PAD_LEFT);
        } else {
            $buffer = $char->getValue();
        }

        $this->debug(
            __METHOD__,
            __LINE__, 
            sprintf('Offset(%u:%u) Char(%s)',
                $this->line,
                $this->column,
                $buffer
            )
        );

        // Increment counters
        $this->offset++;
        $this->column++;
        if ($char->isEOL()) {
            $this->column = 1;
            $this->line++;
        }

        return $char;
    }

    /**
    * Return the last character to the buffer
    * @return Lexer
    */
    public function ungetCharacter() {
        if ($this->offset > 0) {
            $this->offset--;
        }
        if ($this->column > 1) {
            $this->column--;
        }
        
        $this->debug(
            __METHOD__,
            __LINE__, 
            sprintf('Offset(%u:%u)',
                $this->line,
                $this->column
            )
        );

        return $this;
    }


    /**
    * Return the next token and increment all counters
    * @throws SyntaxException
    * @throws EofException
    * @return Token
    */
    public function getToken() {

        try {

            do {
                $char = $this->getCharacter();
            } while ($char->isSpace());

            $token = new Token($char->getLine(), $char->getColumn());
           
            if ($char->isAlpha()) {                 // Found IDENTIFIER
                $this->buildIdentifierToken($token, $char);
            } elseif ($char->isNumber()) {          // Found NUMBER
                $this->buildNumberToken($token, $char);
            } elseif ($char->getValue() == '"') {   // Found STRING
                $this->buildStringToken($token, $char);
            } elseif ($char->isPunct()) {           // Found SYMBOL
                $this->buildSymbolToken($token, $char);
            } else {
                throw new SyntaxException(
                    $char->getLine(),
                    $char->getColumn(), 
                    'Unexpected symbol: ' . $char->getValue(),
                    __FILE__, __LINE__
                );
            }
        // } catch (EofException $e) {
            // throw $e; // pass it on
        } catch (Exception $e) {
            throw $e; // pass it on
            // throw new SyntaxException(
            //     $this->getLine(),
            //     $this->getColumn(), 
            //     'getToken: ' . $e->getMessage(),
            //     __FILE__, __LINE__
            // ); 
        }

        // $this->debug(
        //     __METHOD__,
        //     __LINE__,
        //     sprintf('Found %s: %s', $token->getTypeName(), $token->getValue())
        // );

        // return the token here            
        return $token;
    }

    /*
    * Check if a string value is a lang keyword
    * @return bool
    */
    protected function isLanguageKeyword($value) {
        return in_array($value, $this->keywords);
    }

    /*
    * Return the keywords
    * @return array
    */
    protected function getLanguageKeywords() {
        return $this->keywords;
    }

    // ---------------------- Reusable processors -------------------------

    protected function buildIdentifierToken(Token $token, Character $char) {
        $token->setType(TokenType::IDENTIFIER);
        do {
            $value = $token->getValue();
            $token->setValue($value . $char->getValue());
            try {
                $char = $this->getCharacter();
            } catch (EofException $e) {
                throw new SyntaxException(
                    $char->getLine(), 
                    ($char->getColumn() - mb_strlen($token->getValue())) + 1,
                    "Found EOF reading identifier: '{$token->getValue()}'",
                    __FILE__, __LINE__
                );
            }
        } while (
            $char->isAlnum() || 
            $char->getValue() == '_' || 
            $char->getValue() == '.' /*||
            $char->getValue() == '[' ||   // ARRAY STUFF HERE :/
            $char->getValue() == ']'*/
        );
        $this->ungetCharacter();
        if (    mb_substr($token->getValue(), 0, 1)  == '.' ||
                mb_substr($token->getValue(), mb_strlen($token->getValue()) - 1, 1) == '.')
        {
            throw new SyntaxException(
                $char->getLine(), 
                $char->getColumn(),
                'Identifier can start or end with a dot "' . $token->getValue() . '"',
                __FILE__, __LINE__
            );
        }
        



        $this->debug(__METHOD__, __LINE__, 'Found IDENTIFIER: "' . $token->getValue() . '"');

        if ($token->contains('.')) { // Must be a function call
            if(!$this->getExtensionManager()->resolve($token->getValue())) {
                throw new SyntaxException(
                    $char->getLine(), 
                    $char->getColumn(),
                    'Function not found "' . $token->getValue() . '"',
                    __FILE__, __LINE__
                );
            }
            $this->debug(__METHOD__, __LINE__, 'IDENTIFIER is a method invocation');
            $token->setType(TokenType::MINVOKE);
            return $token;
        }

        if ($this->isLanguageKeyword($token->getValue())) {
            $this->debug(__METHOD__, __LINE__, 'IDENTIFIER is a language keyword');
            $token->setType(TokenType::KEYWORD);
        }

        return $token;
    }

    protected function buildNumberToken(Token $token, Character $char) {
        $token->setType(TokenType::INTEGER);
        $buffer = '';
        do {
            $buffer .= $char->getValue();
            try {
                $char = $this->getCharacter();
            } catch (EofException $e) {
                throw new SyntaxException(
                    $char->getLine(), 
                    ($char->getColumn() - mb_strlen($buffer)) + 1,
                    "Found EOF reading number: '$buffer'",
                    __FILE__, __LINE__
                );
            }
        } while ($char->isNumber() || $char->getValue() == '.');
        $this->ungetCharacter();
        
        // If number contains a period, is a FLOAT
        if (mb_strpos($buffer, '.') !== FALSE) {
            $token->setType(TokenType::FLOAT);
            // If period is not followed by numbers
            if (mb_substr($buffer, -1) == '.') {
                throw new SyntaxException(
                    $token->getLine(), 
                    $token->getColumn(),
                    "Invalid FLOAT value: '$buffer'",
                    __FILE__, __LINE__
                );
            }
        }
    
        switch ($token->getType()) {
            case TokenType::FLOAT:
                $token->setValue(floatval($buffer));
            break;
            case TokenType::INTEGER:
                $token->setValue(intval($buffer));
            break;
            default:
                throw new SyntaxException(
                    $char->getLine(), 
                    $char->getColumn(),
                    'Unknown number token type: "' . $buffer . '"',
                    __FILE__, __LINE__
                );
            break;
        }
        return $token;
    }

    protected function buildStringToken(Token $token, Character $char) {
        $token->setType(TokenType::STRING);
        $buffer = '';
        try {
            while ($char = $this->getCharacter()) {
                if ($char->getValue() == '"') break;
                if ($char->isEOL()) {
                    throw new SyntaxException(
                        $char->getLine(), 
                        $char->getColumn(),
                        'Broken string "' . trim($token->getValue()) . '"',
                        __FILE__, __LINE__
                    );
                }
                $token->setValue(
                    $token->getValue() . $char->getValue()
                );
            }
        } catch (EofException $e) {
            throw new SyntaxException(
                $char->getLine(), 
                ($char->getColumn() - mb_strlen($token->getValue())) + 1,
                "Found EOF reading string: '{$token->getValue()}'",
                __FILE__, __LINE__
            );
        }
        return $token;
    }

    protected function buildSymbolToken(Token $token, Character $char) {
        $token->setType(TokenType::SYMBOL);
        $token->setValue($char->getValue());
        switch ($char->getValue()) {
            // case ';':
            //  break;
            case '(':
            case ')':
                break;
            case '=':
            case '!':
            case '>':
            case '<':
            case '+':
            case '-':
            case '/':
            case '*':
            {
                $prevchar = $char->getValue();
                $char = $this->getCharacter();
                if ($char->isEOL()) {
                    throw new SyntaxException(
                        $char->getLine(), 
                        $char->getColumn(),
                        'Broken expression "' . $prevchar . $char->getValue() . '"',
                        __FILE__, __LINE__
                    );
                } elseif ($prevchar == '/' && $char->getValue() == '/') {
                    $token->setValue($prevchar . $char->getValue());
                } elseif ($char->getValue() == '=') {
                    $token->setValue($prevchar . $char->getValue());
                } elseif ($prevchar == '+' && $char->getValue() == '+') {
                    $token->setValue($prevchar . $char->getValue());
                } elseif ($prevchar == '-' && $char->getValue() == '-') {
                    $token->setValue($prevchar . $char->getValue());
                } else {
                    $this->ungetCharacter();
                }
            }
            break;
            case '&':
            case '|':
            {
                $prevchar = $char->getValue();
                $char = $this->getCharacter();
                if ($char->isEOL()) {
                    throw new SyntaxException(
                        $char->getLine(), 
                        $char->getColumn(),
                        'Invalid expression "' . $prevchar . '"',
                        __FILE__, __LINE__
                    );
                } elseif ($prevchar == $char->getValue()) {
                    $token->setValue($prevchar . $token->getValue());
                } else {
                    $this->ungetCharacter();
                }
            }
            break;
            default:
            break;
        }
        return $token;
    }


}
