<?php
namespace Poop;
/**
* Implementation of Run
*
* This class handles the output for web and cli
*
* @package Poop
*/
class Run
extends Interpreter
implements Run_API
{
    /**
    * Construct an instance
    * @param array $options Optional array of name/value options
    * @see Options
    */
    public function __construct(array $options = NULL) {
        parent::__construct($options);
    }

    /**
    * Run this action and return a result object
    * @return RunResult
    */
    public function run() {
        return $this->doAction('Run');
    }
}