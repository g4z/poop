<?php
namespace Poop;

/**
* Class that implements a hash table to provide the "memory"
* @package Poop
*/
class Memory
extends Object
implements Memory_API
{
    /**
    * @var array $data Array for memory hash
    */  
    protected $data;

    /**
    * Construct the Memory object
    */
    public function __construct() {
        $this->reset();
    }

    /**
    * Initialise the memory with some constants
    */
    public function reset() {
        $this->data = array();
    }

    /**
    * Return the memory data array
    * @return array
    */
    public function getData() { return $this->data; }

    /**
    * Explicitly set the memory data array
    * @param array $data Memory array to set
    * @return Memory
    */
    public function setData(array $data) {
        $this->data = $data;
        return $this;
    }

    /**
    * Return true if key variable is sets
    * @param string $key The hash table key
    * @return bool
    */
    public function hasVar($key) {
        return isset($this->data[$key]);
    }

    /**
    * Get the value for key variable
    * @param string $key The hash table key
    * @return mixed The key variable value or NULL
    */
    public function getVar($key) {
        if(isset($this->data[$key])) {
            return $this->data[$key];
        }
    }
    
    /**
    * Set the value of key variable
    * @param string $key The hash table key
    * @param string $value The value for this key
    * @param integer $type The type integer (@see Type)
    * @return Memory
    */
    public function setVar($key, $value, $type) {
        $this->data[$key] = new MemoryVariable($key, $value, $type);
        return $this;
    }
}
