<?php
namespace Poop;
/**
 * Implement the Options class
 * @package Poop
 */
class Options
extends Object
implements Options_API
{
    /**
    * @var array $options Hash of name/value options
    */
    protected $options;
    
    /**
    * Return the option name
    * @param array $options Optional initial options
    * @return string
    */
    public function __construct(array $options = NULL) {
        $this->options = array();
        if ($options) {
            $this->options = $options;
        }
    }

    /**
    * Return the number of set options
    * @return bool
    */
    public function getCount() { return count($this->options); }

    /**
    * Return true if option exists
    * @param string $name The option name to check
    * @return bool
    */
    public function hasOption($name) { 
        return isset($this->options[$name]);
    }

    /**
    * Return the Option object
    * @param string $name The option name
    * @return mixed Option object or NULL
    */
    public function getOption($name) {
        if (isset($this->options[$name])) {
            return empty($this->options[$name])
                ? TRUE
                : $this->options[$name];
        }
        return NULL;
    }

    /**
    * Set an new Option object
    * @param string $name The option name
    * @param string $value The option value
    * @return Options
    */  
    public function setOption($name, $value) {
        $this->options[$name] = $value;
        return $this;
    }

    /**
    * Return all options
    * @return array An array of Option objects
    */
    public function getOptions() { return $this->options; }

    /**
    * Explicity set all options
    * @param array $options Options array
    * @return Options
    */  
    public function setOptions(array $options) {
        $this->options = $options;
        return $this;
    }
}