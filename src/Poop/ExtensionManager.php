<?php
namespace Poop;
/**
* Implement the Environment class
* @package Poop
*/
class ExtensionManager
extends Object
// implements ExtensionManager_API
{
    private $parent;
    private $instances;
    private $mapping;

    public function __construct(Interpreter $parent) {
        $this->parent = $parent;
        $this->instances = array();
        $this->mapping = array();
    }

    /**
     * Load all extension classes
     */
    public function load() {
        $counter = 0;
        foreach (glob(__DIR__ . '/Extension/*.php') as $filename) {
            $file = new \SplFileInfo($filename);
            $classname = $file->getBasename('.php');
            $classpath = sprintf(
                '%s\Extension\%s',
                __NAMESPACE__,
                $classname
            );
            require_once $filename;
            $extension = new \ReflectionClass($classpath);
            $instance = $extension->newInstance($this->getParent());            
            foreach ($extension->getMethods() as $method) {
                if (mb_substr($method->name, 0, 3) == 'FN_') {
                    $map = new \StdClass;
                    $map->class = $extension->getShortName();
                    $map->method = $method->name;
                    $map->instance = $instance;
                    $tag = sprintf(
                        '%s.%s',
                        mb_strtolower($classname),
                        mb_substr($method->name, 3)
                    );
                    $this->mapping[$tag] = $map;
                }
            }
            $this->debug(
                __METHOD__,
                __LINE__,
                sprintf('Loaded extension: %s', $extension->getShortName())
            );

            // save the instance
            $this->instances[] = $instance;

            $counter++;
        }

        return $counter;
    }

    /**
     * @return array [ VALUE, VARTYPE ]
     */
    public function call($method, $arguments) {
        $realname = 'FN_' . mb_substr($method, mb_strpos($method, '.') + 1);
        if ($this->resolve($method)) {
            
        }
        $mapping = $this->getMapping();
        return $mapping[$method]->instance->{$realname}($arguments);
    }

    public function resolve($method) {
        return isset($this->mapping[$method]);
    }

    public function exports() {
        return array_keys($this->getMapping());
    }

    protected function getParent() {
        return $this->parent;
    }

    protected function getMapping() {
        return $this->mapping;
    }

    public function getExtensions() {
        return $this->instances;
    }

    // public function __construct(Parser $app) {
    //     $this->debug(__METHOD__, __LINE__, 'Loading: ' . get_class($this));
    //     $this->app = $app;
    // }

    // public function __toString() {
    //     $cname = str_replace('Poop\Extension\\', '', get_class($this));
    //     return $cname . '(' . join(', ', $this->exports()) . ')';
    // }

    // public function exports() {
    //     $exports = array();
    //     foreach (get_class_methods($this) as $fn) {
    //         if (mb_substr($fn, 0, 3) == 'FN_') {
    //             $name = substr($fn, 3);
    //             $exports[] = $name;
    //         }
    //     }
    //     // $this->debug(__METHOD__, __LINE__, $exports);
    //     return $exports;
    // }

    // protected function getApp() {
    //     return $this->app;
    // }

    // protected function getMemory() {
    //     return $this->app->getMemory();
    // }

}