<?php
namespace Poop;
/**
* Interface definition for a Tag object
* @package Poop
*/
interface Tag_API extends Object_API {
    
    /**
    * Return the string value of the token
    * @return string
    */
    function getValue();

    /**
    * Set the value of the token
    * @param string $value The string value to set
    * @return Tag
    */
    function setValue($value);

    /**
    * Return the source code line number of the token
    * @return int
    */
    function getLine();

    /**
    * Return the source code column number of the token
    * @return int
    */
    function getColumn();

    /**
    * Return an array representation of this token
    * @return array
    */
    function toArray();

    /**
    * Return true if the token value string is empty
    * @return bool
    */
    function isEmpty();

    /**
    * Return true if the token value string is alpha numeric
    * @return bool
    */
    function isAlnum();

    /**
    * Return true if the token value string is alphabetic
    * @return bool
    */
    function isAlpha();

    /**
    * Return true if the token value is a number
    * @return bool
    */
    function isNumber();

    /**
    * Return true if the token value is a control character
    * @return bool
    */
    function isCtrl();

    /**
    * Return true if token string is printable
    * @return bool
    */
    function isPrint();

    /**
    * Return true if token string is printable with no spaces
    * @return bool
    */
    function isGraph();

    /**
    * Return true if token string is lowercase
    * @return bool
    */
    function isLower();

    /**
    * Return true if token string is uppercase
    * @return bool
    */
    function isUpper();

    /**
    * Return true if token string is punctuation
    * @return bool
    */
    function isPunct();

    /**
    * Return true if token string is whitespace
    * @return bool
    */
    function isSpace();

    /**
    * Return true if token is hexidecimal
    * @return bool
    */
    function isHex();
}