<?php
namespace Poop;
/**
* Interface definition for a Engine object
* @package Poop
*/
interface Engine_API
extends Parser_API
{
    /**
    * Construct the Engine object
    * @param array Array of name/value options
    */
    function __construct(array $options = NULL);

    /**
    * Set internal buffer and update option --code
    * @param string $code The source code string
    * @return Engine
    */
    function setCode($code);

    /**
    * Get the source code string buffer
    * @return string
    */
    function getCode();

    /**
    * Handle the execute action
    * @return string The output from the script
    */
    function doRun();

    /**
    * Handle the syntax check option
    */
    function doSyntaxCheck();

    /**
    * Handle the tokeniser
    * @return array An array of Tokens
    */
    function doTokenise();

    /**
    * Handle the minimiser
    */
    function doMinimise();
    
    /**
    * Return the engine options
    * @return Options
    */
    function getOptions();

    /**
    * Return an engine option
    * @param array $names Array of option names
    * @return mixed
    */
    function getOption(array $names);

    /**
    * Return the engine options
    * @param string $name Name of the option
    * @param mixed $value Value for the option
    * @return Options
    */
    function setOption($name, $value);
}