<?php
namespace Poop;
/**
* Class to handle read characters from a string buffer
* @package Poop
*/
abstract class Reader
extends Object
implements Reader_API
{
    /**
    * @var string $buffer The string buffer for source code
    */  
    protected $buffer;    // String: Source code string
    
    /**
    * @var int $offset The current string offset value
    */  
    protected $offset;
 
    /**
    * Construct the Reader object
    */   
    public function __construct() {
        $this->buffer = '';
        $this->offset = 0;
    }

    /**
    * Return the character code of the current buffer offset
    * @throws EofException    
    * @return int
    */
    public function getOrd() {
        if ($this->offset < 0 ||
            // $this->offset >= mb_strlen($this->buffer)
            $this->offset >= mb_strlen($this->buffer)
        ) {
            $this->debug(__METHOD__, __LINE__, 'Found EOF at offset: ' . $this->offset);
            throw new EofException();
        }
        $buffer = ord($this->buffer{$this->offset});
        return $buffer;
    }

    /**
    * Return the string buffer
    * @return string
    */
    public function getBuffer() {
        return $this->buffer;
    }
    
    /**
    * Set the string buffer
    * @param string $buffer The source code string
    * @return Reader
    */
    public function setBuffer($buffer) {
        $this->buffer = $buffer;
        $this->offset = 0;
        return $this;
    }

    /**
    * Return the current offset
    * @return int
    */
    public function getOffset() {
        return $this->offset;
    }

    /**
    * Set the current offset
    * @param int $offset The new offset value
    * @return Reader
    */
    public function setOffset($offset) {
        $this->offset = $offset;
        return $this;
    }
}
