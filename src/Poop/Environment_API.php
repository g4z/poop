<?php
namespace Poop;
/**
* Interface definition for a Engine object
* @package Poop
*/
interface Environment_API extends Object_API {

    /**
    * Represents standalone mode
    */
    const MODE_NATIVE = 1;

    /**
    * Represents included mode
    */
    const MODE_HOSTED = 2;

    /**
    * Represents a CLI sapi type
    */
    const ENV_CLI = 1;
    
    /**
    * Represents a non-CLI sapi type
    */
    const ENV_WEB = 2;

    /**
    * Auto-detect the environment settings
    * @return Environment
    */
    function autoDetect();

    /**
    * Return program options from all envs
    * @return array A hash of name:value options
    */
    function getOptions();

    /**
    * Return true if mode is Native
    * @return bool
    */
    function isNative();

    /**
    * Return true if mode is Hosted
    * @return bool
    */
    function isHosted();

    /**
    * Return true if sapi is a web server
    * @return bool
    */
    function isWeb();

    /**
    * Return true if sapi is CLI
    * @return bool
    */
    function isCli();
}