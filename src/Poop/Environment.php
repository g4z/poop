<?php
namespace Poop;
/**
* Implement the Environment class
* @package Poop
*/
class Environment
extends Object
implements Environment_API
{
    /**
    * @var int $env Stores the current environment type
    * @see Environment::ENV_CLI
    * @see Environment::ENV_WEB
    */
    protected $env;

    /**
    * @var int $mode Store the current mode
    * @see Environment::MODE_NATIVE
    * @see Environment::MODE_HOSTED
    */
    protected $mode;

    /**
    * Construct the Environment object
    */
    public function __construct() {
        $this->autoDetect();
    }

    /**
    * Crude check if we are included in another's code
    * @return bool
    */
    public static function isIncluded() {
        $includes = get_included_files();
        return (basename($includes[0]) != 'poop.php');
    }

    /**
    * Auto-detect the environment settings
    * @return Environment
    */
    public function autoDetect() {

        // Detect environment
        switch (mb_strtolower(php_sapi_name())) {
            case 'cli':
                $this->env = self::ENV_CLI;
            break;
            default:
                $this->env = self::ENV_WEB;
            break;
        }

        // Detect mode
        if ($this->isIncluded()) {
            $this->mode = self::MODE_HOSTED;
        } else {
            $this->mode = self::MODE_NATIVE;
        }
    }

    /**
    * Return program options from all envs
    * @return array A hash of name:value options
    */
    function getOptions() {

        $options = array();

        switch ($this->env)
        {
            case self::ENV_WEB:
            {
                foreach (OptionMap::getInstance() as $option)
                {
                    if (isset($_REQUEST[$option[0]]))
                    {
                        $options[$option[0]] = empty($option[2])
                            ? FALSE
                            : $_REQUEST[$option[0]];
                    }
                    elseif(isset($_REQUEST[$option[1]]))
                    {
                        $options[$option[1]] = empty($option[2])
                            ? FALSE
                            : $_REQUEST[$option[1]];
                    }
                }
            }
            break;      

            case self::ENV_CLI:
            default:
            {
                $short = '';
                $long = array();
                foreach (OptionMap::getInstance() as $option) {
                    if (empty($option[2])) { // no val
                        $short .= $option[1];
                        $long[] = $option[0];
                    } else {
                        $short .= $option[1] . ':';
                        $long[] = $option[0] . ':';
                    }
                }
                $options = getopt($short, $long);
            }
            break;
        }
        return $options;
    }

    /**
    * Return true if mode is Native
    * @return bool
    */
    public function isNative() {
        return ($this->mode == self::MODE_NATIVE);
    }

    /**
    * Return true if mode is Hosted
    * @return bool
    */
    public function isHosted() {
        return ($this->mode == self::MODE_HOSTED);
    }

    /**
    * Return true if sapi is a web server
    * @return bool
    */
    public function isWeb() {
        return ($this->env == self::ENV_WEB);
    }

    /**
    * Return true if sapi is CLI
    * @return bool
    */
    public function isCli() {
        return ($this->env == self::ENV_CLI);
    }
}