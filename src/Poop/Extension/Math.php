<?php
namespace Poop\Extension;

use Poop\Variable;
use Poop\Type;
use Poop\Extension;

/**
* Implement the Maths extension
* @package Poop
*/
class Math
extends Extension
// implements Math_API
{
    public function FN_ceil(array $arguments) {
        list($input) = $this->parseArguments(
            $arguments, 
            1, 
            array(Type::FLOAT)
        );
        $output = ceil($input);
        return new Variable($output, Type::INTEGER);
    }

    public function FN_floor(array $arguments) {
        list($input) = $this->parseArguments(
            $arguments, 
            1, 
            array(Type::FLOAT)
        );
        $output = floor($input);
        return new Variable($output, Type::INTEGER);
    }

}