<?php
namespace Poop\Extension;

use Poop\ArgumentException;
use Poop\Variable;
use Poop\Type;
use Poop\Extension;

/**
* Implement the HTTP extension
* @package Poop
*/
class Http
extends Extension
// implements Http_API
{
    public function FN_get(array $arguments) {

        list($url) = $this->parseArguments(
            $arguments, 
            1,
            array(Type::STRING)
        );

        if (!filter_var($url, FILTER_VALIDATE_URL)) {
            throw new ArgumentException("Not a valid URL: '$url'");
        }

        
        $buffer = @file_get_contents($url);

        if ($buffer === false) {
            return new Variable(NULL, Type::NULL);
        }
        
        return new Variable($buffer, Type::STRING);
    }

}
