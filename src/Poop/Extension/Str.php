<?php
namespace Poop\Extension;

use Poop\Variable;
use Poop\Type;
use Poop\Extension;

/**
* Implement the String extension
* @package Poop
*/
class Str
extends Extension
// implements String_API
{
    public function FN_repeat(array $arguments) {
        list($buffer, $multiplier) = $this->parseArguments(
            $arguments, 
            2,
            array(
                Type::STRING,
                Type::INTEGER,
            )
        );
        $buffer = str_repeat($buffer, $multiplier);
        return new Variable($buffer, Type::STRING);        
    }

    public function FN_replace(array $arguments) {
        list($haystack, $needle, $replacement) = $this->parseArguments(
            $arguments, 
            3, 
            array(
                Type::STRING,
                Type::STRING,
                Type::STRING,
            )
        );
        $buffer = str_replace($needle, $replacement, $haystack);
        return new Variable($buffer, Type::STRING);
    }

    public function FN_trim(array $arguments) {
        list($buffer) = $this->parseArguments(
            $arguments,
            1,
            array(Type::STRING)
        );
        $buffer = trim($buffer);
        return new Variable($buffer, Type::STRING);
    }

}