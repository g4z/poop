<?php
namespace Poop\Extension;

use Poop\ArgumentException;
use Poop\Variable;
use Poop\Type;
use Poop\Extension;

/**
* Implement the File extension
* @package Poop
*/
class File
extends Extension
// implements File_API
{
    public function FN_read(array $arguments) {
        list($filepath) = $this->parseArguments(
            $arguments, 
            1,
            array(Type::STRING)
        );
        if (!is_readable($filepath)) {
            throw new ArgumentException("File not readable: '$filepath'");
        }
        return new Variable(file_get_contents($filepath), Type::STRING);
    }

    public function FN_write(array $arguments) {
        list($filepath, $data) = $this->parseArguments(
            $arguments, 
            2,
            array(Type::STRING, Type::STRING)
        );
        if (!is_writable($filepath)) {
            throw new ArgumentException("File not writable: '$filepath'");
        }
        return new Variable(file_put_contents($filepath, $data), Type::INTEGER);
    }

    public function FN_append(array $arguments) {
        list($filepath, $data) = $this->parseArguments(
            $arguments, 
            2,
            array(Type::STRING, Type::STRING)
        );
        if (!is_writable($filepath)) {
            throw new ArgumentException("File not writable: '$filepath'");
        }
        return new Variable(file_put_contents($filepath, $data, FILE_APPEND), Type::INTEGER);
    }

    public function FN_prepend(array $arguments) {
        list($filepath, $data) = $this->parseArguments(
            $arguments, 
            2,
            array(Type::STRING, Type::STRING)
        );
        if (!is_writable($filepath)) {
            throw new ArgumentException("File not writable: '$filepath'");
        }
        return new Variable(file_put_contents($filepath, $data . file_get_contents($filepath)), Type::INTEGER);
    }

    public function FN_touch(array $arguments) {
        list($filepath) = $this->parseArguments(
            $arguments, 
            1,
            array(Type::STRING)
        );
        if (!is_writable(dirname($filepath))) {
            throw new ArgumentException("Filepath not writable: '$filepath'");
        }
        return new Variable(touch($filepath), Type::BOOLEAN);
    }

    public function FN_remove(array $arguments) {
        list($filepath) = $this->parseArguments(
            $arguments, 
            1,
            array(Type::STRING)
        );
        if (!is_writable($filepath)) {
            throw new ArgumentException("Filepath not writable: '$filepath'");
        }
        return new Variable(unlink($filepath), Type::BOOLEAN);
    }
}
