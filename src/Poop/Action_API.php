<?php
namespace Poop;
/**
* Definition of an Interpreter Action class
* @package Poop
*/
interface Action_API extends Object_API {

    /**
    * Run this action and return a result object
    * @return Result
    */
    function run();
}