<?php
/**
* Poop main
* @package Poop
*/

// Development/debug
function dd()
{
    foreach (func_get_args() as $arg) {
        echo print_r($arg, true), PHP_EOL;
    }
    exit;
}

function __autoload($name) {
    $name = '/' . strtr($name, '\\', '/') . '.php';
    include __DIR__ . $name;
}

// Doesn't work if we're in a .phar file
if (!Poop\Environment::isIncluded()) {
    $poop = new Poop\Dispatcher();
    $poop->run();
}
